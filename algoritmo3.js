stage = new createjs.Stage("canvas"); //CRIA O PALCO
stage.enableMouseOver(10);//ATIVA O MOUSEOVER

//Elementos da tela de progresso
var telaDeProgresso = new createjs.Container();

var barraProgresso = new createjs.Bitmap("imagens/load/bar.png");
barraProgresso.x = 200;
barraProgresso.y = 275;

var fundoTelaBarraProgresso = new createjs.Shape();
fundoTelaBarraProgresso.graphics.beginFill("#333333").drawRect(0,0,800,600);

var fundoBarraProgresso = new createjs.Shape();
fundoBarraProgresso.graphics.beginFill("#ffb91e").drawRect(200, 275, 400, 50);

var carregando = new createjs.Text("Carregando...", "bold 20px Oxygen", "#ffffff");
carregando.textAlign = "center";
carregando.x = 400;
carregando.y = 245;

var textoBarraProgresso = new createjs.Text("0%", "bold 24px Oxygen", "#000000");
textoBarraProgresso.textAlign = "center";
textoBarraProgresso.x = 400;
textoBarraProgresso.y = 290;

telaDeProgresso.addChild(fundoTelaBarraProgresso, fundoBarraProgresso, barraProgresso, textoBarraProgresso, carregando);
stage.addChild(telaDeProgresso);
//--------------------------------------

//Carregamento dos arquivos
var arquivos = new createjs.LoadQueue(false);
arquivos.on("progress", loadBar, this);
arquivos.on("complete", completo, this);
//MUITO IMPORTANTE!!!
//Instala o plugin para tocar os áudios
arquivos.installPlugin(createjs.Sound);

arquivos.loadManifest([
	//Menu inicial
	{id: "menu", src:"imagens/menu_inicial/fundo.png"},
	{id: "jogar", src:"imagens/menu_inicial/jogar.png"},
	{id: "somBtn", src:"imagens/menu_inicial/somBtn.png"},
	{id: "ajudaBtn", src:"imagens/menu_inicial/ajudaBtn.png"},
	{id: "fundoCreditos", src:"imagens/menu_inicial/creditos.png"},
	{id: "fundoAjuda", src:"imagens/menu_inicial/ajuda.png"},
	{id: "random", src:"imagens/menu_inicial/random.png"},
	{id: "nossoSite", src:"imagens/menu_inicial/nossoSite.png"},

	//Animação inicial
	{id:"canario", src:"imagens/animacao/canario.png"},
	{id:"canarioFeliz", src:"imagens/animacao/canarioFeliz.png"},
	{id:"canarioRaiva", src:"imagens/animacao/canarioRaiva.png"},
	{id:"fundoAnimacao", src:"imagens/animacao/fundo.png"},
	{id:"caixaAnimacao", src:"imagens/animacao/caixa.png"},
	{id:"setinha", src:"imagens/animacao/seta.png"},

	//Cenas do tutorial
	{id: "proximoTutorial", src: "imagens/tutorial/proximo.png"},
	{id: "pularTutorial", src: "imagens/tutorial/pularTutorial.png"},
	{id: "brasilTutorial", src: "imagens/tutorial/brasil.png"},
	{id: "bibliotecaTutorial", src: "imagens/tutorial/biblioteca.png"},
	{id: "fundoTutorial01", src: "imagens/tutorial/fundo01.png"},
	{id: "fundoTutorial02", src: "imagens/tutorial/fundo02.png"},
	{id: "fundoTutorial03", src: "imagens/tutorial/fundo03.png"},
	{id: "fundoTutorial04", src: "imagens/tutorial/fundo04.png"},
	{id: "fundoTutorial05", src: "imagens/tutorial/fundo05.png"},
	{id: "fundoTutorial06", src: "imagens/tutorial/fundo06.png"},
	{id: "fundoTutorial07", src: "imagens/tutorial/fundo07.png"},
	{id: "fundoTutorial08", src: "imagens/tutorial/fundo08.png"},
	{id: "fundoTutorial09", src: "imagens/tutorial/fundo09.png"},
	{id: "fundoTutorial10", src: "imagens/tutorial/fundo10.png"},
	{id: "fundoTutorial11", src: "imagens/tutorial/fundo11.png"},
	{id: "fundoTutorial12", src: "imagens/tutorial/fundo12.png"},
	{id: "fundoTutorial13", src: "imagens/tutorial/fundo13.png"},
	{id: "fundoTutorial14", src: "imagens/tutorial/fundo14.png"},
	{id: "fundoTutorial15", src: "imagens/tutorial/fundo15.png"},
	{id: "fundoTutorial16", src: "imagens/tutorial/fundo16.png"},
	{id: "fundoTutorial17", src: "imagens/tutorial/fundo17.png"},
	{id: "fundoTutorial18", src: "imagens/tutorial/fundo18.png"},

	//Cenários
	{id: "biblioteca", src:"imagens/cenarios/biblioteca.png"},
	{id: "loja", src:"imagens/cenarios/loja.png"},
	{id: "mercado", src:"imagens/cenarios/mercado.png"},
	{id: "museu", src:"imagens/cenarios/museu.png"},
	{id: "aeroporto", src:"imagens/cenarios/aeroporto.png"},
	{id: "placa-aeroporto", src:"imagens/cenarios/placa-aeroporto.png"},
	{id: "caixa-dialogo-aeroporto", src:"imagens/cenarios/caixa-dialogo-aeroporto.png"},
	{id: "informacao-mexico", src:"imagens/cenarios/informacao-mexico.png"},
	{id: "informacao-venezuela", src:"imagens/cenarios/informacao-venezuela.png"},
	{id: "informacao-brasil", src:"imagens/cenarios/informacao-brasil.png"},
	{id: "informacao-chile", src:"imagens/cenarios/informacao-chile.png"},
	{id: "informacao-argentina", src:"imagens/cenarios/informacao-argentina.png"},

	//Personagens que compoem as cenas (NPCs)
	{id: "placeholder", src: "imagens/cenarios/personagens/placeholder.png"},
	{id: "personagem00", src: "imagens/cenarios/personagens/00.png"},
	{id: "personagem01", src: "imagens/cenarios/personagens/01.png"},
	{id: "personagem02", src: "imagens/cenarios/personagens/02.png"},
	{id: "personagem03", src: "imagens/cenarios/personagens/03.png"},
	{id: "personagem04", src: "imagens/cenarios/personagens/04.png"},
	{id: "personagem10", src: "imagens/cenarios/personagens/10.png"},
	{id: "personagem11", src: "imagens/cenarios/personagens/11.png"},
	{id: "personagem12", src: "imagens/cenarios/personagens/12.png"},
	{id: "personagem13", src: "imagens/cenarios/personagens/13.png"},
	{id: "personagem14", src: "imagens/cenarios/personagens/14.png"},
	{id: "personagem20", src: "imagens/cenarios/personagens/20.png"},
	{id: "personagem21", src: "imagens/cenarios/personagens/21.png"},
	{id: "personagem22", src: "imagens/cenarios/personagens/22.png"},
	{id: "personagem23", src: "imagens/cenarios/personagens/23.png"},
	{id: "personagem24", src: "imagens/cenarios/personagens/24.png"},
	{id: "personagem30", src: "imagens/cenarios/personagens/30.png"},
	{id: "personagem31", src: "imagens/cenarios/personagens/31.png"},
	{id: "personagem32", src: "imagens/cenarios/personagens/32.png"},
	{id: "personagem33", src: "imagens/cenarios/personagens/33.png"},
	{id: "personagem34", src: "imagens/cenarios/personagens/34.png"},
	{id: "personagem40", src: "imagens/cenarios/personagens/40.png"},
	{id: "personagem41", src: "imagens/cenarios/personagens/41.png"},
	{id: "personagem42", src: "imagens/cenarios/personagens/42.png"},
	{id: "personagem43", src: "imagens/cenarios/personagens/43.png"},
	{id: "personagem44", src: "imagens/cenarios/personagens/44.png"},

	//Objetos que compoem as cenas
	{id: "objetoplaceholder", src: "imagens/cenarios/objetos/placeholder.png"},
	{id: "objeto01", src: "imagens/cenarios/objetos/01.png"},
	{id: "objeto02", src: "imagens/cenarios/objetos/02.png"},
	{id: "objeto03", src: "imagens/cenarios/objetos/03.png"},
	{id: "objeto04", src: "imagens/cenarios/objetos/04.png"},
	{id: "objeto11", src: "imagens/cenarios/objetos/11.png"},
	{id: "objeto12", src: "imagens/cenarios/objetos/12.png"},
	{id: "objeto13", src: "imagens/cenarios/objetos/13.png"},
	{id: "objeto14", src: "imagens/cenarios/objetos/14.png"},
	{id: "objeto21", src: "imagens/cenarios/objetos/21.png"},
	{id: "objeto22", src: "imagens/cenarios/objetos/22.png"},
	{id: "objeto23", src: "imagens/cenarios/objetos/23.png"},
	{id: "objeto24", src: "imagens/cenarios/objetos/24.png"},
	{id: "objeto31", src: "imagens/cenarios/objetos/31.png"},
	{id: "objeto32", src: "imagens/cenarios/objetos/32.png"},
	{id: "objeto33", src: "imagens/cenarios/objetos/33.png"},
	{id: "objeto34", src: "imagens/cenarios/objetos/34.png"},
	{id: "objeto41", src: "imagens/cenarios/objetos/41.png"},
	{id: "objeto42", src: "imagens/cenarios/objetos/42.png"},
	{id: "objeto43", src: "imagens/cenarios/objetos/43.png"},
	{id: "objeto44", src: "imagens/cenarios/objetos/44.png"},

	//Interface
	//-------- Superior
	{id: "caixaLocalizacao", src:"imagens/interface/menu_superior/caixaLocalizacao.png"},
	{id: "caixaPerfil", src:"imagens/interface/menu_superior/perfil.png"},
	{id: "medalhaCinza", src:"imagens/interface/menu_superior/medalhaCinza.png"},
	{id: "medalhaColorida", src:"imagens/interface/menu_superior/medalhaColorida.png"},
	{id: "relogio", src:"imagens/interface/menu_superior/tempo.png"},
	//-------- Botões
	{id: "dialogo", src:"imagens/interface/caixa_dialogo/caixa.png"},
	{id: "setaDialogo", src:"imagens/interface/caixa_dialogo/seta.png"},
	{id: "btnMenu", src:"imagens/interface/botoes/icone-menu.png"},
	{id: "btnMapa", src:"imagens/interface/botoes/icone-mapa.png"},
	{id: "btnRotas", src:"imagens/interface/botoes/icone-lugar.png"},
	{id: "btnMissao", src:"imagens/interface/botoes/icone-missao.png"},
	{id: "btnSuspeitos", src:"imagens/interface/botoes/icone-suspeitos.png"},
	{id: "btnMandado", src:"imagens/interface/botoes/icone-mandado.png"},
	//-------- Legendas
	{id: "legendaMenu", src:"imagens/interface/legenda/menu.png"},
	{id: "legendaMapa", src:"imagens/interface/legenda/mapa.png"},
	{id: "legendaLugares", src:"imagens/interface/legenda/lugares.png"},
	{id: "legendaMissao", src:"imagens/interface/legenda/missao.png"},
	{id: "legendaSuspeitos", src:"imagens/interface/legenda/suspeitos.png"},
	{id: "legendaMandado", src:"imagens/interface/legenda/mandado.png"},
	//-------- Bandeiras
	{id: "iconeMexico", src:"imagens/interface/bandeiras/mexico.png"},
	{id: "iconeVenezuela", src:"imagens/interface/bandeiras/venezuela.png"},
	{id: "iconeBrasil", src:"imagens/interface/bandeiras/brasil.png"},
	{id: "iconeChile", src:"imagens/interface/bandeiras/chile.png"},
	{id: "iconeArgentina", src:"imagens/interface/bandeiras/argentina.png"},

	//Tablet
	{id: "tablet", src:"imagens/interface/tablet/tablet.png"},
	{id: "direita", src:"imagens/interface/tablet/direita.png"},
	{id: "esquerda", src:"imagens/interface/tablet/esquerda.png"},

	//Mapa
	{id: "brasil", src:"imagens/interface/mapa/mapabrasil.png"},
	{id: "mexico", src:"imagens/interface/mapa/mapamexico.png"},
	{id: "venezuela", src:"imagens/interface/mapa/mapavenezuela.png"},
	{id: "argentina", src:"imagens/interface/mapa/mapaargentina.png"},
	{id: "chile", src:"imagens/interface/mapa/mapachile.png"},
	{id: "nomebrasil", src:"imagens/interface/mapa/brasil.png"},
	{id: "nomemexico", src:"imagens/interface/mapa/mexico.png"},
	{id: "nomevenezuela", src:"imagens/interface/mapa/venezuela.png"},
	{id: "nomeargentina", src:"imagens/interface/mapa/argentina.png"},
	{id: "nomechile", src:"imagens/interface/mapa/chile.png"},
	{id: "fundomapa", src:"imagens/interface/mapa/fundomapa.png"},
	{id: "pontomapa", src:"imagens/interface/mapa/pontomapa.png"},
	{id: "pinmapa", src:"imagens/interface/mapa/pinmapa.png"},

	//Lugares
	{id: "fundolugares", src:"imagens/interface/lugares/fundo.png"},
	{id: "icnaeroporto", src:"imagens/interface/lugares/icnaeroporto.png"},
	{id: "icnloja", src:"imagens/interface/lugares/icnloja.png"},
	{id: "icnbiblioteca", src:"imagens/interface/lugares/icnbiblioteca.png"},
	{id: "icnmercado", src:"imagens/interface/lugares/icnmercado.png"},
	{id: "icnmuseu", src:"imagens/interface/lugares/icnmuseu.png"},
	{id: "pontolugares", src:"imagens/interface/lugares/ponto.png"},
	{id: "pinlugar", src:"imagens/interface/lugares/pinlugar.png"},

	//Suspeitos
	{id: "fundosuspeitos", src:"imagens/interface/suspeitos/fundo.png"},
	{id: "botaoProximaTela", src:"imagens/interface/suspeitos/botaoProxima.png"},
	{id: "botaoTelaAnterior", src:"imagens/interface/suspeitos/botaoAnterior.png"},
	{id: "breno", src:"imagens/suspeitos/breno.png"},
	{id: "bruno", src:"imagens/suspeitos/bruno.png"},
	{id: "gabriel", src:"imagens/suspeitos/gabriel.png"},
	{id: "hiago", src:"imagens/suspeitos/hiago.png"},
	{id: "julianne", src:"imagens/suspeitos/julianne.png"},
	{id: "brenoCircular", src:"imagens/interface/suspeitos/brenoCircular.png"},
	{id: "brunoCircular", src:"imagens/interface/suspeitos/brunoCircular.png"},
	{id: "gabrielCircular", src:"imagens/interface/suspeitos/gabrielCircular.png"},
	{id: "hiagoCircular", src:"imagens/interface/suspeitos/hiagoCircular.png"},
	{id: "julianneCircular", src:"imagens/interface/suspeitos/julianneCircular.png"},
	{id: "brenofinal", src:"imagens/suspeitos/brenofinal.png"},
	{id: "brunofinal", src:"imagens/suspeitos/brunofinal.png"},
	{id: "gabrielfinal", src:"imagens/suspeitos/gabrielfinal.png"},
	{id: "hiagofinal", src:"imagens/suspeitos/hiagofinal.png"},
	{id: "juliannefinal", src:"imagens/suspeitos/juliannefinal.png"},

	//Missão
	{id: "fundomissao", src:"imagens/interface/missao/fundo.png"},

	//Mandado
	{id: "fundomandado", src:"imagens/interface/mandado/fundo.png"},
	{id: "desconhecido", src:"imagens/interface/mandado/desconhecido.png"},

	//Menuzinho do jogo (opções)
	{id: "fundomenu", src:"imagens/interface/opcoes/fundo.png"},
	{id: "continuar", src:"imagens/interface/opcoes/continuar.png"},
	{id: "voltar", src:"imagens/interface/opcoes/voltar.png"},
	{id: "recomecar", src:"imagens/interface/opcoes/recomecar.png"},
	{id: "som", src:"imagens/interface/opcoes/som.png"},
	{id: "semSom", src:"imagens/interface/opcoes/semSom.png"},

	//Brasões
	{id: "fundobrasao", src:"imagens/interface/brasao/fundo.png"},
	{id: "nenhum", src:"imagens/interface/brasao/nenhum.png"},
	{id: "nenhumGrande", src:"imagens/interface/brasao/nenhumGrande.png"},
	{id: "aguia", src:"imagens/interface/brasao/aguia.png"},
	{id: "aguiaGrande", src:"imagens/interface/brasao/aguiaGrande.png"},
	{id: "fenix", src:"imagens/interface/brasao/fenix.png"},
	{id: "fenixGrande", src:"imagens/interface/brasao/fenixGrande.png"},
	{id: "pinto", src:"imagens/interface/brasao/pinto.png"},
	{id: "pintoGrande", src:"imagens/interface/brasao/pintoGrande.png"},
	{id: "canarioBrasao", src:"imagens/interface/brasao/canario.png"},
	{id: "canarioBrasaoGrande", src:"imagens/interface/brasao/canarioGrande.png"},

	//TELA DE MISSÃO
	{id: "missao", src:"imagens/cenarios/missao.png"},

	//SONS DO JOGO
	{id: "powerup", src: "sons/powerup.mp3"},
	{id: "musicaDeFundo", src: "sons/musicafundomenu.mp3"}
]);

//Variáveis globais
var precisaTutorial = true;
var p=0;
var alvo=0;
var l;
local=[];
var cenario=[];
var menu={};
var jogo={};
var tempo=0;
var missao = {};
var mapa={};
var tablet={};
var suspeitos = [];
var nomeDoDetetive = "Jovem Detetive";
var suspeitoMandado = -1;
var animacaoInicial = {};
var animacaoFinal = {};
var musicaDeFundo;
local[0]={};
local[1]={};
local[2]={};
local[3]={};
local[4]={};
var telafinal = {};

var culpado = 0;
var esconderijo = 0;

function completo (){//ESTA FUNCAO DEIXA BOA PARTE DOS OBJETOS E CONTAINERS PRONTOS PARA USO.
	//Remove a tela de carregamento
	stage.removeAllChildren();

	//TUTORIAL
	//* Essa função está no arquivo tutorial.js
	criaCenasTutorial();

	//ANIMAÇÃO INICIAL
	animacaoInicial.tela = new createjs.Container();
	animacaoInicial.fundo = new createjs.Bitmap(arquivos.getResult("fundoAnimacao"));
	animacaoInicial.tablet = {};
	animacaoInicial.tablet.comp = new createjs.Container();
	animacaoInicial.tablet.tela = new createjs.Bitmap(arquivos.getResult("tablet"));
	animacaoInicial.suspeitoFinal = new createjs.Bitmap(arquivos.getResult("objetoplaceholder"));
	animacaoInicial.canario = new createjs.Bitmap(arquivos.getResult("canario"));
	animacaoInicial.canarioFeliz = new createjs.Bitmap(arquivos.getResult("canarioFeliz"));
	animacaoInicial.canarioRaiva = new createjs.Bitmap(arquivos.getResult("canarioRaiva"));
	animacaoInicial.caixa = new createjs.Bitmap(arquivos.getResult("caixaAnimacao"));
	animacaoInicial.maodireita = new createjs.Bitmap(arquivos.getResult("direita"));
	animacaoInicial.maoesquerda = new createjs.Bitmap(arquivos.getResult("esquerda"));
	animacaoInicial.setinha = new createjs.Bitmap(arquivos.getResult("setinha"));
	animacaoInicial.investigar = new createjs.Bitmap(arquivos.getResult("jogar"));

	//Textos do inspetor canário
	animacaoInicial.falas = [];
	animacaoInicial.numFala = 0;
	animacaoInicial.falas[0] = "Olá, Jovem. Seu desempenho em nossa escola é considerável.";
	animacaoInicial.falas[1] = "Seus níveis de percepção são excelentes, e é interesse nosso integrá-lo à nossa divisão de investigação.";
	animacaoInicial.falas[2] = "Você será integrado temporariamente à Organização de Investigação da América Latina.";
	animacaoInicial.falas[3] = "Você terá de provar seu valor para compor permanentemente nossa equipe.";
	animacaoInicial.falas[4] = "Através deste tablet você receberá todos os informes necessários para executar com excelência suas missões.";
	animacaoInicial.falas[5] = "Está preparado, jovem detetive?";

	//Posicionando os elementos da animação inicial
	animacaoInicial.tablet.tela.regX = animacaoInicial.tablet.tela.getBounds().width/2;
	animacaoInicial.tablet.tela.x = stage.canvas.width/2;
	animacaoInicial.tablet.tela.y = stage.canvas.height;

	animacaoInicial.canario.regX = animacaoInicial.canario.getBounds().width/2;
	animacaoInicial.canario.x = animacaoInicial.tablet.tela.x;
	animacaoInicial.canario.y = animacaoInicial.tablet.tela.y + 52;

	animacaoInicial.canarioFeliz.regX = animacaoInicial.canarioFeliz.getBounds().width/2;
	animacaoInicial.canarioFeliz.x = animacaoInicial.tablet.tela.x;
	animacaoInicial.canarioFeliz.y = animacaoInicial.tablet.tela.y + 52;
	animacaoInicial.canarioFeliz.visible = false;

	animacaoInicial.canarioRaiva.regX = animacaoInicial.canarioRaiva.getBounds().width/2;
	animacaoInicial.canarioRaiva.x = animacaoInicial.tablet.tela.x;
	animacaoInicial.canarioRaiva.y = animacaoInicial.tablet.tela.y + 52;
	animacaoInicial.canarioRaiva.visible = false;

	animacaoInicial.caixa.regX = animacaoInicial.caixa.getBounds().width/2;
	animacaoInicial.caixa.x = animacaoInicial.tablet.tela.x;
	animacaoInicial.caixa.y = 985;
	animacaoInicial.caixa.alpha = 0;

	animacaoInicial.texto = new createjs.Text("", "16px Oxygen", "#ffffff");
	animacaoInicial.texto.lineWidth = 296;
	animacaoInicial.texto.lineHeight = 20;
	animacaoInicial.texto.x = animacaoInicial.caixa.x - 146;
	animacaoInicial.texto.y = animacaoInicial.caixa.y + 44;

	animacaoInicial.setinha.x = animacaoInicial.caixa.x + 115;
	animacaoInicial.setinha.y = animacaoInicial.caixa.y + 86;
	animacaoInicial.setinha.visible = false;

	animacaoInicial.investigar.regX = animacaoInicial.investigar.getBounds().width/2;
	animacaoInicial.investigar.x = animacaoInicial.caixa.x;
	animacaoInicial.investigar.y = animacaoInicial.caixa.y + 18;
	animacaoInicial.investigar.scaleX = 0.7;
	animacaoInicial.investigar.scaleY = 0.7;
	animacaoInicial.investigar.alpha = 0;

	animacaoInicial.maodireita.y = 820;
	animacaoInicial.maodireita.x = -6;

	animacaoInicial.tablet.comp.addChild(animacaoInicial.tablet.tela, animacaoInicial.canario,
		animacaoInicial.canarioFeliz, animacaoInicial.canarioRaiva,	animacaoInicial.caixa,
		animacaoInicial.texto, animacaoInicial.setinha, animacaoInicial.maodireita, animacaoInicial.investigar);

	animacaoInicial.tela.addChild(animacaoInicial.fundo, animacaoInicial.suspeitoFinal, animacaoInicial.tablet.comp);
	animacaoInicial.tela.alpha = 0;

	//FINAL
	telafinal.perdeu = new createjs.Bitmap(arquivos.getResult("perdeu"));
	telafinal.ganhou = new createjs.Bitmap(arquivos.getResult("ganhou"));
	telafinal.errou = new createjs.Bitmap(arquivos.getResult("errou"));

	//IMAGEM DE INFORMAÇÕES DOS PAÍSES (VAI FICAR NA PLACA DO AEROPORTO)
	pais[0].informacao = new createjs.Bitmap(arquivos.getResult("informacao-mexico"));
	pais[1].informacao = new createjs.Bitmap(arquivos.getResult("informacao-venezuela"));
	pais[2].informacao = new createjs.Bitmap(arquivos.getResult("informacao-brasil"));
	pais[3].informacao = new createjs.Bitmap(arquivos.getResult("informacao-chile"));
	pais[4].informacao = new createjs.Bitmap(arquivos.getResult("informacao-argentina"));

	//PERSONAGENS DE CADA LUGAR (SÃO 25)
	pais[0].npc[0] = new createjs.Bitmap(arquivos.getResult("personagem00"));
	pais[0].npc[1] = new createjs.Bitmap(arquivos.getResult("personagem01"));
	pais[0].npc[2] = new createjs.Bitmap(arquivos.getResult("personagem02"));
	pais[0].npc[3] = new createjs.Bitmap(arquivos.getResult("personagem03"));
	pais[0].npc[4] = new createjs.Bitmap(arquivos.getResult("personagem04"));
	pais[1].npc[0] = new createjs.Bitmap(arquivos.getResult("personagem10"));
	pais[1].npc[1] = new createjs.Bitmap(arquivos.getResult("personagem11"));
	pais[1].npc[2] = new createjs.Bitmap(arquivos.getResult("personagem12"));
	pais[1].npc[3] = new createjs.Bitmap(arquivos.getResult("personagem13"));
	pais[1].npc[4] = new createjs.Bitmap(arquivos.getResult("personagem14"));
	pais[2].npc[0] = new createjs.Bitmap(arquivos.getResult("personagem20"));
	pais[2].npc[1] = new createjs.Bitmap(arquivos.getResult("personagem21"));
	pais[2].npc[2] = new createjs.Bitmap(arquivos.getResult("personagem22"));
	pais[2].npc[3] = new createjs.Bitmap(arquivos.getResult("personagem23"));
	pais[2].npc[4] = new createjs.Bitmap(arquivos.getResult("personagem24"));
	pais[3].npc[0] = new createjs.Bitmap(arquivos.getResult("personagem30"));
	pais[3].npc[1] = new createjs.Bitmap(arquivos.getResult("personagem31"));
	pais[3].npc[2] = new createjs.Bitmap(arquivos.getResult("personagem32"));
	pais[3].npc[3] = new createjs.Bitmap(arquivos.getResult("personagem33"));
	pais[3].npc[4] = new createjs.Bitmap(arquivos.getResult("personagem34"));
	pais[4].npc[0] = new createjs.Bitmap(arquivos.getResult("personagem40"));
	pais[4].npc[1] = new createjs.Bitmap(arquivos.getResult("personagem41"));
	pais[4].npc[2] = new createjs.Bitmap(arquivos.getResult("personagem42"));
	pais[4].npc[3] = new createjs.Bitmap(arquivos.getResult("personagem43"));
	pais[4].npc[4] = new createjs.Bitmap(arquivos.getResult("personagem44"));

	pais[0].npc[0].nome = new createjs.Text("Comissária", "bold 19px Oxygen", "#ffffff");
	pais[0].npc[1].nome = new createjs.Text("Vendedora", "bold 19px Oxygen", "#ffffff");
	pais[0].npc[2].nome = new createjs.Text("Curador", "bold 19px Oxygen", "#ffffff");
	pais[0].npc[3].nome = new createjs.Text("Feirante", "bold 19px Oxygen", "#ffffff");
	pais[0].npc[4].nome = new createjs.Text("Bibliotecário", "bold 19px Oxygen", "#ffffff");
	pais[1].npc[0].nome = new createjs.Text("Comissária", "bold 19px Oxygen", "#ffffff");
	pais[1].npc[1].nome = new createjs.Text("Vendedor", "bold 19px Oxygen", "#ffffff");
	pais[1].npc[2].nome = new createjs.Text("Curadora", "bold 19px Oxygen", "#ffffff");
	pais[1].npc[3].nome = new createjs.Text("Feirante", "bold 19px Oxygen", "#ffffff");
	pais[1].npc[4].nome = new createjs.Text("Bibliotecária", "bold 19px Oxygen", "#ffffff");
	pais[2].npc[0].nome = new createjs.Text("Comissária", "bold 19px Oxygen", "#ffffff");
	pais[2].npc[1].nome = new createjs.Text("Vendedora", "bold 19px Oxygen", "#ffffff");
	pais[2].npc[2].nome = new createjs.Text("Curadora", "bold 19px Oxygen", "#ffffff");
	pais[2].npc[3].nome = new createjs.Text("Feirante", "bold 19px Oxygen", "#ffffff");
	pais[2].npc[4].nome = new createjs.Text("Bibliotecário", "bold 19px Oxygen", "#ffffff");
	pais[3].npc[0].nome = new createjs.Text("Comissária", "bold 19px Oxygen", "#ffffff");
	pais[3].npc[1].nome = new createjs.Text("Vendedora", "bold 19px Oxygen", "#ffffff");
	pais[3].npc[2].nome = new createjs.Text("Curador", "bold 19px Oxygen", "#ffffff");
	pais[3].npc[3].nome = new createjs.Text("Feirante", "bold 19px Oxygen", "#ffffff");
	pais[3].npc[4].nome = new createjs.Text("Bibliotecária", "bold 19px Oxygen", "#ffffff");
	pais[4].npc[0].nome = new createjs.Text("Comissária", "bold 19px Oxygen", "#ffffff");
	pais[4].npc[1].nome = new createjs.Text("Vendedora", "bold 19px Oxygen", "#ffffff");
	pais[4].npc[2].nome = new createjs.Text("Curador", "bold 19px Oxygen", "#ffffff");
	pais[4].npc[3].nome = new createjs.Text("Feirante", "bold 19px Oxygen", "#ffffff");
	pais[4].npc[4].nome = new createjs.Text("Bibliotecária", "bold 19px Oxygen", "#ffffff");

	/*
	------- Essa parte do código será utilizada quando tivermos todos os personagens
	------- Até lá, usaremos o código que está abaixo (descomentado)
	for(var i=0; i<pais.length; i++){
	
	}
	*/

	for(var i=0; i<pais.length; i++){
		for(var j=0; j<pais.length; j++){
			if(pais[i].npc[j] == undefined){
				pais[i].npc[j] = new createjs.Bitmap(arquivos.getResult("placeholder"));
				pais[i].npc[j].nome = new createjs.Text("Personagem", "27px AgencyFB", "#ffffff");
			}
			pais[i].npc[j].regX = pais[i].npc[j].getBounds().width/2;
			pais[i].npc[j].regY = pais[i].npc[j].getBounds().height;

			switch(j){
				case 0:
					pais[i].npc[j].x = 500;
					pais[i].npc[j].y = 398.8;
					break;
				case 1:
					pais[i].npc[j].x = 570;
					pais[i].npc[j].y = stage.canvas.height+2;
					break;
				case 2:
					pais[i].npc[j].x = 300;
					pais[i].npc[j].y = stage.canvas.height+2;
					break;
				case 3:
					pais[i].npc[j].x = 330;
					pais[i].npc[j].y = stage.canvas.height+2;
					break;
				case 4:
					pais[i].npc[j].x = 320;
					pais[i].npc[j].y = 403;
					break;
			}
		}
	}
	pais[2].npc[4].x = 310;
	pais[2].npc[4].y = 435;
	pais[3].npc[4].y = 425;

	//OBJETOS DE CADA LUGAR
	//É um código geral para inserir todos os objetos de todos os cenários do jogo
	//O primeiro "for" percorre cada país, enquanto que o segundo percorre o vetor
	//"objeto" de cada país. Em seguida, checa qual o índice do vetor "objeto" e,
	//dependendo dessa informação, posiciona a imagem em um ou outro lugar na tela.
	for(var i=0; i<pais.length; i++){
		for(var j=0; j<pais.length; j++){
			if(j>0){
				pais[i].objeto[j] = new createjs.Bitmap(arquivos.getResult("objeto"+i+""+j));
			}else{
				//Esse else é importante porque lá embaixo, na função "investiga", nós utilizamos
				//uma chamada a "pais[p].objeto[l]", e como nós não temos objetos no aeroporto
				//(índice 0), é necessário inserir um placeholder nele, para que não dê erro na chamada
				pais[i].objeto[j] = new createjs.Bitmap(arquivos.getResult("objetoplaceholder"));
			}

			pais[i].objeto[j].regX = pais[i].objeto[j].getBounds().width/2;
			pais[i].objeto[j].regY = pais[i].objeto[j].getBounds().height;

			switch(j){
				case 1:
					pais[i].objeto[j].x = 259;
					pais[i].objeto[j].y = 439;
					break;
				case 2:
					pais[i].objeto[j].x = stage.canvas.width/2-24.2;
					pais[i].objeto[j].y = stage.canvas.height-11;
					break;
				case 3:
					pais[i].objeto[j].x = 600;
					pais[i].objeto[j].y = 491;
					break;
				case 4:
					pais[i].objeto[j].x = 535;
					pais[i].objeto[j].y = 530;
					break;
			}
		}
	}

	//TELA DE MISSÃO
	jogo.telamissao = new createjs.Bitmap(arquivos.getResult("missao"));

	//TABLET
	tablet.comp = new createjs.Container();
	tablet.tela = new createjs.Bitmap(arquivos.getResult("tablet"));
	parametrizar(tablet.tela);
	tablet.tela.x = canvas.width/2 - tablet.tela.largura/2;
	tablet.maodireita = new createjs.Bitmap(arquivos.getResult("direita"));
	tablet.maoesquerda = new createjs.Bitmap(arquivos.getResult("esquerda"));
	parametrizar(tablet.maodireita);
	parametrizar(tablet.maoesquerda);
	tablet.maodireita.y = canvas.height + 10 - tablet.maodireita.altura;
	tablet.maodireita.x = -6;
	tablet.comp.addChild(tablet.tela);
	tablet.comp.y = canvas.height;

	//TELA DE LUGARES
	local[0].icone = new createjs.Bitmap(arquivos.getResult("icnaeroporto")); 
	local[1].icone = new createjs.Bitmap(arquivos.getResult("icnloja"));
	local[2].icone = new createjs.Bitmap(arquivos.getResult("icnmuseu"));
	local[3].icone = new createjs.Bitmap(arquivos.getResult("icnmercado"));
	local[4].icone = new createjs.Bitmap(arquivos.getResult("icnbiblioteca"));
	
	for (var i = 0; i < local.length; i++) {
		local[i].icone.local = i;
		local[i].icone.on("click", jogo.locais);
		parametrizar(local[i].icone);
	};

	local[0].nome = "Aeroporto";
	local[0].num = 0;
	local[1].nome = "Loja";
	local[1].num = 1;
	local[2].nome = "Museu";
	local[2].num = 2;
	local[3].nome = "Mercado";
	local[3].num = 3;
	local[4].nome = "Biblioteca";
	local[4].num = 4;
	
	cenario[0] = new createjs.Bitmap(arquivos.getResult("aeroporto"));
	cenario[1] = new createjs.Bitmap(arquivos.getResult("loja"));
	cenario[2] = new createjs.Bitmap(arquivos.getResult("museu"));
	cenario[3] = new createjs.Bitmap(arquivos.getResult("mercado"));
	cenario[4] = new createjs.Bitmap(arquivos.getResult("biblioteca"));

	for (var i = 0; i < cenario.length; i++) {
		parametrizar(cenario[i]);
		ajustar(cenario[i], 0, 0, canvas.width, canvas.height);
		parametrizar(local[i].icone);
	}

	pais[0].icone = new createjs.Bitmap(arquivos.getResult("iconeMexico"));
	pais[1].icone = new createjs.Bitmap(arquivos.getResult("iconeVenezuela"));
	pais[2].icone = new createjs.Bitmap(arquivos.getResult("iconeBrasil"));
	pais[3].icone = new createjs.Bitmap(arquivos.getResult("iconeChile"));
	pais[4].icone = new createjs.Bitmap(arquivos.getResult("iconeArgentina"));

	for(var i=0; i<pais.length; i++){
		pais[i].icone.on("mouseover", jogo.mostraLocalizacao, this);
		pais[i].icone.on("mouseout", jogo.mostraLocalizacao, this);
	}

	//CAIXA DE DIÁLOGO
	jogo.dialogo = new createjs.Bitmap(arquivos.getResult("dialogo"));

	for (var i = 0; i < pais.length; i++) {
		parametrizar(pais[i].icone);
		pais[i].icone.regX = pais[i].icone.getBounds().width;
		pais[i].icone.x = canvas.width-20.5;
		pais[i].icone.y = 23.5;
	}

	//PARTE SUPERIOR
	jogo.perfil = new createjs.Container();

	jogo.caixaPerfil = new createjs.Bitmap(arquivos.getResult("caixaPerfil"));
	parametrizar(jogo.caixaPerfil);
	jogo.caixaPerfil.x = 25;
	jogo.caixaPerfil.y = 25;
	jogo.perfil.addChild(jogo.caixaPerfil);

	jogo.brasoes = [];
	jogo.brasoesConquistados = [];
	jogo.brasoes[0] = new createjs.Bitmap(arquivos.getResult("pinto"));
	jogo.brasoes[0].grande = new createjs.Bitmap(arquivos.getResult("pintoGrande"));
	jogo.brasoes[1] = new createjs.Bitmap(arquivos.getResult("canarioBrasao"));
	jogo.brasoes[1].grande = new createjs.Bitmap(arquivos.getResult("canarioBrasaoGrande"));
	jogo.brasoes[2] = new createjs.Bitmap(arquivos.getResult("aguia"));
	jogo.brasoes[2].grande = new createjs.Bitmap(arquivos.getResult("aguiaGrande"));
	jogo.brasoes[3] = new createjs.Bitmap(arquivos.getResult("fenix"));
	jogo.brasoes[3].grande = new createjs.Bitmap(arquivos.getResult("fenixGrande"));

	for(var i=0; i<jogo.brasoes.length; i++){
		jogo.brasoes[i].regX = jogo.brasoes[i].getBounds().width/2;
		jogo.brasoes[i].regY = jogo.brasoes[i].getBounds().height/2;
		jogo.brasoes[i].x = jogo.caixaPerfil.x + 34;
		jogo.brasoes[i].y = jogo.caixaPerfil.y + 34;
	}

	for(var i=0; i<jogo.brasoes.length; i++){
		jogo.brasoesConquistados[i] = new createjs.Bitmap(arquivos.getResult("nenhum"));
		jogo.brasoesConquistados[i].grande = new createjs.Bitmap(arquivos.getResult("nenhumGrande"));
		jogo.brasoesConquistados[i].regX = jogo.brasoesConquistados[i].getBounds().width/2;
		jogo.brasoesConquistados[i].regY = jogo.brasoesConquistados[i].getBounds().height/2;
		jogo.brasoesConquistados[i].x = jogo.caixaPerfil.x + 34;
		jogo.brasoesConquistados[i].y = jogo.caixaPerfil.y + 36;
	}

	jogo.brasaoAtual = jogo.brasoesConquistados[0];
	jogo.brasaoAtual.num = 0;
	jogo.brasaoAtual.box = new createjs.Container();
	jogo.fundobrasao = new createjs.Bitmap(arquivos.getResult("fundobrasao"));
	jogo.fundobrasao.x = tablet.tela.x + 28;
	jogo.fundobrasao.y = tablet.tela.y + 52;
	jogo.fundobrasao.on("click", fazNada);
	jogo.brasaoAtual.box.addChild(jogo.fundobrasao);

	for(var i=0; i<jogo.brasoesConquistados.length; i++){
		jogo.brasoesConquistados[i].grande.regX = jogo.brasoesConquistados[i].grande.getBounds().width/2;
		jogo.brasoesConquistados[i].grande.regY = jogo.brasoesConquistados[i].grande.getBounds().height/2;
		jogo.brasoesConquistados[i].grande.x = tablet.tela.x + 123 + 158*(i%2);
		if(i>1){
			jogo.brasoesConquistados[i].grande.y = tablet.tela.y + 225 + 172;
		}else{
			jogo.brasoesConquistados[i].grande.y = tablet.tela.y + 225;
		}
		jogo.brasaoAtual.box.addChild(jogo.brasoesConquistados[i].grande);
	}

	jogo.brasaoAtual.on("click", tela, this);
	jogo.brasaoAtual.on("mouseover", tela, this);
	jogo.brasaoAtual.on("mouseout", tela, this);

	jogo.nomeDoDetetive = new createjs.Text(nomeDoDetetive, "29px AgencyFB", "#ffffff");
	jogo.nomeDoDetetive.x = jogo.caixaPerfil.x + 80;
	jogo.nomeDoDetetive.y = jogo.caixaPerfil.y + 7.5;
	jogo.perfil.addChild(jogo.nomeDoDetetive);

	jogo.medalhas = [];
	jogo.medalhasConquistadas = [false, false, false, false];

	for(var i=0; i<4; i++){
		if(jogo.medalhasConquistadas[i] == true){
			jogo.medalhas[i] = new createjs.Bitmap(arquivos.getResult("medalhaColorida"));	
		}
		else{
			jogo.medalhas[i] = new createjs.Bitmap(arquivos.getResult("medalhaCinza"));
		}
		jogo.medalhas[i].x = jogo.nomeDoDetetive.x + 20*i;
		jogo.medalhas[i].y = jogo.nomeDoDetetive.y + 29;
		jogo.perfil.addChild(jogo.medalhas[i]);
	}

	jogo.relogio = new createjs.Bitmap(arquivos.getResult("relogio"));
	jogo.relogio.x = jogo.nomeDoDetetive.x + 88;
	jogo.relogio.y = jogo.nomeDoDetetive.y + 31;
	jogo.perfil.addChild(jogo.relogio);

	jogo.tempoRestante = new createjs.Text(tempo+":00", "27px AgencyFB", "#ffffff");
	jogo.tempoRestante.x = jogo.relogio.x + 25;
	jogo.tempoRestante.y = jogo.nomeDoDetetive.y + 29;
	jogo.perfil.addChild(jogo.tempoRestante);

	jogo.localizacao = new createjs.Container();
	jogo.localizacao.visible =false;

	jogo.caixaLocalizacao = new createjs.Bitmap(arquivos.getResult("caixaLocalizacao"));
	parametrizar(jogo.caixaLocalizacao);
	jogo.caixaLocalizacao.x = canvas.width - 285;
	jogo.caixaLocalizacao.y = 26;
	jogo.localizacao.addChild(jogo.caixaLocalizacao);

	//NOMES DOS PAISES
	for (var i=0; i<pais.length; i++){
		pais[i].titulo = new createjs.Text(pais[i].capital+", "+pais[i].nome, "27px AgencyFB", "#ffffff");
		pais[i].titulo.x = jogo.caixaLocalizacao.x + 16;
		pais[i].titulo.y = jogo.caixaLocalizacao.y + 7.5;
	}

	//MENU
	jogo.menu = new createjs.Container();

	jogo.menubtn = [];
	jogo.menubtn[0] = new createjs.Bitmap(arquivos.getResult("btnMapa"));
	jogo.menubtn[1] = new createjs.Bitmap(arquivos.getResult("btnRotas"));
	jogo.menubtn[2] = new createjs.Bitmap(arquivos.getResult("btnMissao"));
	jogo.menubtn[3] = new createjs.Bitmap(arquivos.getResult("btnSuspeitos"));
	jogo.menubtn[4] = new createjs.Bitmap(arquivos.getResult("btnMandado"));
	jogo.menubtn[5] = new createjs.Bitmap(arquivos.getResult("btnMenu"));

	for (var i=0; i<6; i++){
		parametrizar(jogo.menubtn[i]);
		jogo.menubtn[i].regX = jogo.menubtn[i].getBounds().width/2;
		jogo.menubtn[i].x = canvas.width - 57.5;
		jogo.menubtn[i].y = 110+i*(jogo.menubtn[i].getBounds().height+15.7);
		jogo.menubtn[i].num = i;
		jogo.menubtn[i].on("mouseover", mostraLegenda);
		jogo.menubtn[i].on("mouseout", mostraLegenda);
		jogo.menu.addChild(jogo.menubtn[i]);
	}
	jogo.menubtn[5].y = canvas.height-(jogo.menubtn[5].getBounds().height + 25);

	//BOTOES
	jogo.menubtn[0].ativado = false;
	jogo.menubtn[1].ativado = false;
	jogo.menubtn[2].ativado = false;
	jogo.menubtn[3].ativado = false;
	jogo.menubtn[4].ativado = false;
	jogo.menubtn[5].ativado = false;

	jogo.menubtn[0].nome = "Mapa";
	jogo.menubtn[1].nome = "Rotas";
	jogo.menubtn[2].nome = "Missão";
	jogo.menubtn[3].nome = "Suspeitos";
	jogo.menubtn[4].nome = "Mandado de Prisão";
	jogo.menubtn[5].nome = "Opções";

	jogo.menubtn[0].box = new createjs.Container();
	jogo.menubtn[1].box = new createjs.Container();
	jogo.menubtn[2].box = new createjs.Container();
	jogo.menubtn[3].box = new createjs.Container();
	jogo.menubtn[4].box = new createjs.Container();

	//Legenda
	jogo.legendabtn = [];
	jogo.legendabtn[0] = new createjs.Bitmap(arquivos.getResult("legendaMapa"));
	jogo.legendabtn[1] = new createjs.Bitmap(arquivos.getResult("legendaLugares"));
	jogo.legendabtn[2] = new createjs.Bitmap(arquivos.getResult("legendaMissao"));
	jogo.legendabtn[3] = new createjs.Bitmap(arquivos.getResult("legendaSuspeitos"));
	jogo.legendabtn[4] = new createjs.Bitmap(arquivos.getResult("legendaMandado"));
	jogo.legendabtn[5] = new createjs.Bitmap(arquivos.getResult("legendaMenu"));

	for(var i=0; i<6; i++){
		parametrizar(jogo.legendabtn[i]);
		jogo.legendabtn[i].regX = jogo.legendabtn[i].getBounds().width;
		jogo.legendabtn[i].regY = jogo.legendabtn[i].getBounds().height/2;
		jogo.legendabtn[i].x = jogo.menubtn[i].x - 32;
		jogo.legendabtn[i].y = jogo.menubtn[i].y +jogo.menubtn[i].getBounds().height/2;
		jogo.legendabtn[i].visible = false;
		jogo.menu.addChild(jogo.legendabtn[i]);
	}

	//MAPA
	mapa.fundo = new createjs.Bitmap(arquivos.getResult("fundomapa"));
	mapa.fundo.x = tablet.tela.x + 34;
	mapa.fundo.y = tablet.tela.y + 52;
	mapa.fundo.on("click", fazNada, this);
	mapa.comp = new createjs.Container();
	mapa.pin = new createjs.Bitmap(arquivos.getResult("pinmapa"));
	mapa.comp.addChild(mapa.fundo);

	pais[0].mapa = new createjs.Bitmap(arquivos.getResult("mexico"));
	pais[0].mapa.x = 262;
	pais[0].mapa.y = 147;
	pais[1].mapa = new createjs.Bitmap(arquivos.getResult("venezuela"));
	pais[1].mapa.x = 420;
	pais[1].mapa.y = 223;
	pais[2].mapa = new createjs.Bitmap(arquivos.getResult("brasil"));
	pais[2].mapa.x = 418;
	pais[2].mapa.y = 248;
	pais[3].mapa = new createjs.Bitmap(arquivos.getResult("chile"));
	pais[3].mapa.x = 376;
	pais[3].mapa.y = 329;
	pais[4].mapa = new createjs.Bitmap(arquivos.getResult("argentina"));
	pais[4].mapa.x = 421;
	pais[4].mapa.y = 345;

	//OS PONTOS DO MAPA FORAM ADICIONADOS
	mapa.ponto = [];
	mapa.nomePais = [];
	mapa.nomePais[0] = new createjs.Bitmap(arquivos.getResult("nomemexico"));
	mapa.nomePais[0].regX = mapa.nomePais[0].getBounds().width/2;
	mapa.nomePais[1] = new createjs.Bitmap(arquivos.getResult("nomevenezuela"));
	mapa.nomePais[1].regX = mapa.nomePais[1].getBounds().width/2;
	mapa.nomePais[2] = new createjs.Bitmap(arquivos.getResult("nomebrasil"));
	mapa.nomePais[2].regX = mapa.nomePais[2].getBounds().width/2;
	mapa.nomePais[3] = new createjs.Bitmap(arquivos.getResult("nomechile"));
	mapa.nomePais[3].regX = mapa.nomePais[3].getBounds().width/2;
	mapa.nomePais[4] = new createjs.Bitmap(arquivos.getResult("nomeargentina"));
	mapa.nomePais[4].regX = mapa.nomePais[4].getBounds().width/2;

	for (var a = 0; a < pais.length; a++) {
		mapa.ponto[a] = new createjs.Bitmap(arquivos.getResult("pontomapa"));
		pais[a].mapa.pais = a;
		mapa.ponto[a].pais = a;
		pais[a].mapa.on("click", jogo.pais);
		mapa.comp.addChild(pais[a].mapa, mapa.ponto[a]);
	}

	mapa.comp.y = -canvas.height*0.02;
	mapa.comp.x = -canvas.width*0.0075;

	mapa.ponto[0].x = canvas.width*0.394;
	mapa.ponto[1].x = canvas.width*0.556;
	mapa.ponto[2].x = canvas.width*0.625;
	mapa.ponto[3].x = canvas.width*0.534;
	mapa.ponto[4].x = canvas.width*0.557;

	mapa.nomePais[0].x = mapa.ponto[0].x + 5;
	mapa.nomePais[1].x = mapa.ponto[1].x + 5;
	mapa.nomePais[2].x = mapa.ponto[2].x + 5;
	mapa.nomePais[3].x = mapa.ponto[3].x + 5;
	mapa.nomePais[4].x = mapa.ponto[4].x + 5;

	mapa.ponto[0].y = canvas.height*0.306;
	mapa.ponto[1].y = canvas.height*0.400;
	mapa.ponto[2].y = canvas.height*0.525;
	mapa.ponto[3].y = canvas.height*0.605;
	mapa.ponto[4].y = canvas.height*0.673;

	mapa.nomePais[0].y = mapa.ponto[0].y + 15;
	mapa.nomePais[1].y = mapa.ponto[1].y + 15;
	mapa.nomePais[2].y = mapa.ponto[2].y + 15;
	mapa.nomePais[3].y = mapa.ponto[3].y + 15;
	mapa.nomePais[4].y = mapa.ponto[4].y + 15;

	mapa.comp.addChild(mapa.nomePais[0], mapa.nomePais[1], mapa.nomePais[2], mapa.nomePais[3], mapa.nomePais[4]);

	mapa.comp.addChild(mapa.pin);
	mapa.pin.x = mapa.ponto[p].x - canvas.width*0.019;
	mapa.pin.y = mapa.ponto[p].y - canvas.height*0.04;

	parametrizar(mapa.comp);

	//TELA DE SUSPEITOS
	//-------- Criando os objetos que representam os suspeitos
	suspeitos[0] = {};
	suspeitos[1] = {};
	suspeitos[2] = {};
	suspeitos[3] = {};
	suspeitos[4] = {};

	suspeitos[0].nome = "Breno Silva";
	suspeitos[0].fotoPequena = new createjs.Bitmap(arquivos.getResult("brenoCircular"));
	suspeitos[0].fotoGrande = new createjs.Bitmap(arquivos.getResult("breno"));
	suspeitos[0].fotoFinal = new createjs.Bitmap(arquivos.getResult("brenofinal"));
	suspeitos[0].caracteristica = [];
	suspeitos[0].caracteristica[0] = "cabelo castanho.";
	suspeitos[0].caracteristica[1] = "olhos castanhos escuros.";
	suspeitos[0].caracteristica[2] = "boca grande.";
	suspeitos[0].caracteristica[3] = "pele morena.";

	suspeitos[1].nome = "Bruno Garcia";
	suspeitos[1].fotoPequena = new createjs.Bitmap(arquivos.getResult("brunoCircular"));
	suspeitos[1].fotoGrande = new createjs.Bitmap(arquivos.getResult("bruno"));
	suspeitos[1].fotoFinal = new createjs.Bitmap(arquivos.getResult("brunofinal"));
	suspeitos[1].caracteristica = [];
	suspeitos[1].caracteristica[0] = "cabelo preto.";
	suspeitos[1].caracteristica[1] = "olhos castanhos médios.";
	suspeitos[1].caracteristica[2] = "boca pequena.";
	suspeitos[1].caracteristica[3] = "pele branca.";
	
	suspeitos[2].nome = "Gabriel Sanchez";
	suspeitos[2].fotoPequena = new createjs.Bitmap(arquivos.getResult("gabrielCircular"));
	suspeitos[2].fotoGrande = new createjs.Bitmap(arquivos.getResult("gabriel"));
	suspeitos[2].fotoFinal = new createjs.Bitmap(arquivos.getResult("gabrielfinal"));
	suspeitos[2].caracteristica = [];
	suspeitos[2].caracteristica[0] = "cabelo castanho escuro.";
	suspeitos[2].caracteristica[1] = "olhos castanhos médios.";
	suspeitos[2].caracteristica[2] = "boca grande.";
	suspeitos[2].caracteristica[3] = "pele parda.";
	
	suspeitos[3].nome = "Hiago Ramirez";
	suspeitos[3].fotoPequena = new createjs.Bitmap(arquivos.getResult("hiagoCircular"));
	suspeitos[3].fotoGrande = new createjs.Bitmap(arquivos.getResult("hiago"));
	suspeitos[3].fotoFinal = new createjs.Bitmap(arquivos.getResult("hiagofinal"));
	suspeitos[3].caracteristica = [];
	suspeitos[3].caracteristica[0] = "cabelo castanho escuro.";
	suspeitos[3].caracteristica[1] = "olhos castanhos médios.";
	suspeitos[3].caracteristica[2] = "boca pequena.";
	suspeitos[3].caracteristica[3] = "pele parda.";

	suspeitos[4].nome = "Julianne Gonzalez";
	suspeitos[4].fotoPequena = new createjs.Bitmap(arquivos.getResult("julianneCircular"));
	suspeitos[4].fotoGrande = new createjs.Bitmap(arquivos.getResult("julianne"));
	suspeitos[4].fotoFinal = new createjs.Bitmap(arquivos.getResult("juliannefinal"));
	suspeitos[4].caracteristica = [];
	suspeitos[4].caracteristica[0] = "cabelo preto.";
	suspeitos[4].caracteristica[1] = "olhos castanhos escuros.";
	suspeitos[4].caracteristica[2] = "boca pequena.";
	suspeitos[4].caracteristica[3] = "pele branca.";

	//------ Agora adicionando o resto
	jogo.telaSuspeitos1 = new createjs.Container();
	jogo.telaSuspeitos2 = new createjs.Container();

	jogo.fundosuspeitos = new createjs.Bitmap(arquivos.getResult("fundosuspeitos"));
	jogo.fundosuspeitos.on("click", fazNada, this);
	jogo.fundosuspeitos.x = tablet.tela.x + 28;
	jogo.fundosuspeitos.y = tablet.tela.y + 52;
	
	jogo.botaoProximaTela = new createjs.Bitmap(arquivos.getResult("botaoProximaTela"));
	jogo.botaoProximaTela.x = tablet.tela.x + 311;
	jogo.botaoProximaTela.y = tablet.tela.y + 68;
	jogo.botaoProximaTela.num = 0;
	jogo.botaoProximaTela.on("click", jogo.mudaTelaSuspeitos);

	jogo.telaSuspeitos1.addChild(jogo.botaoProximaTela);

	//Insere os primeiros três suspeitos da tela do tablet
	for(var i=0; i<3; i++){
		suspeitos[i].fotoPequena.x = tablet.tela.x + 60;
		suspeitos[i].fotoPequena.y = tablet.tela.y + 155 + (suspeitos[i].fotoPequena.getBounds().height + 57)*i;
		suspeitos[i].fotoPequena.num = i;
		suspeitos[i].fotoPequena.on("click", mostraFotoGrande, this);

		var nomeSuspeito = new createjs.Text(suspeitos[i].nome, "bold 16px Oxygen", "#222222");
		nomeSuspeito.x = suspeitos[i].fotoPequena.x + suspeitos[i].fotoPequena.getBounds().width + 20;
		nomeSuspeito.y = suspeitos[i].fotoPequena.y - 5;
		var descricaoSuspeito = new createjs.Text(primeiraLetraMaiuscula(suspeitos[i].caracteristica[0])+"\n"+
			primeiraLetraMaiuscula(suspeitos[i].caracteristica[1])+"\n"+
			primeiraLetraMaiuscula(suspeitos[i].caracteristica[2])+"\n"+
			primeiraLetraMaiuscula(suspeitos[i].caracteristica[3]), "14px Oxygen", "#222222");
		descricaoSuspeito.lineWidth = 205;
		descricaoSuspeito.x = nomeSuspeito.x;
		descricaoSuspeito.y = nomeSuspeito.y + 25;
		jogo.telaSuspeitos1.addChild(suspeitos[i].fotoPequena, nomeSuspeito, descricaoSuspeito);
	}

	for(var i=0; i<3; i++){
		suspeitos[i].fotoGrande.x = suspeitos[i].fotoPequena.x + suspeitos[i].fotoPequena.getBounds().width/2;
		suspeitos[i].fotoGrande.y = suspeitos[i].fotoPequena.y + suspeitos[i].fotoPequena.getBounds().height/2;
		suspeitos[i].fotoGrande.scaleX = 0;
		suspeitos[i].fotoGrande.scaleY = 0;
		jogo.telaSuspeitos1.addChild(suspeitos[i].fotoGrande);
	}

	jogo.botaoTelaAnterior = new createjs.Bitmap(arquivos.getResult("botaoTelaAnterior"));
	jogo.botaoTelaAnterior.x = tablet.tela.x + 48;
	jogo.botaoTelaAnterior.y = tablet.tela.y + 68;
	jogo.botaoTelaAnterior.num = 1;
	jogo.botaoTelaAnterior.on("click", jogo.mudaTelaSuspeitos);

	jogo.telaSuspeitos2.addChild(jogo.botaoTelaAnterior);

	//Insere os dois últimos suspeitos da tela do tablet
	for(var i=3; i<5; i++){
		suspeitos[i].fotoPequena.x = tablet.tela.x + 60;
		suspeitos[i].fotoPequena.y = tablet.tela.y + 155 + (suspeitos[i].fotoPequena.getBounds().height + 57)*(i-3);
		suspeitos[i].fotoPequena.num = i;
		suspeitos[i].fotoPequena.on("click", mostraFotoGrande, this);

		var nomeSuspeito = new createjs.Text(suspeitos[i].nome, "bold 16px Oxygen", "#222222");
		nomeSuspeito.x = suspeitos[i].fotoPequena.x + suspeitos[i].fotoPequena.getBounds().width + 20;
		nomeSuspeito.y = suspeitos[i].fotoPequena.y - 5;
		var descricaoSuspeito = new createjs.Text(primeiraLetraMaiuscula(suspeitos[i].caracteristica[0])+"\n"+
			primeiraLetraMaiuscula(suspeitos[i].caracteristica[1])+"\n"+
			primeiraLetraMaiuscula(suspeitos[i].caracteristica[2])+"\n"+
			primeiraLetraMaiuscula(suspeitos[i].caracteristica[3]), "14px Oxygen", "#222222");
		descricaoSuspeito.lineWidth = 205;
		descricaoSuspeito.x = nomeSuspeito.x;
		descricaoSuspeito.y = nomeSuspeito.y + 25;
		jogo.telaSuspeitos2.addChild(suspeitos[i].fotoPequena, nomeSuspeito, descricaoSuspeito);
	}

	for(var i=3; i<5; i++){
		suspeitos[i].fotoGrande.x = suspeitos[i].fotoPequena.x + suspeitos[i].fotoPequena.getBounds().width/2;
		suspeitos[i].fotoGrande.y = suspeitos[i].fotoPequena.y + suspeitos[i].fotoPequena.getBounds().height/2;
		suspeitos[i].fotoGrande.scaleX = 0;
		suspeitos[i].fotoGrande.scaleY = 0;
		jogo.telaSuspeitos2.addChild(suspeitos[i].fotoGrande);
	}

	jogo.telaSuspeitos2.visible = false;

	jogo.menubtn[3].box.addChild(jogo.fundosuspeitos, jogo.telaSuspeitos1, jogo.telaSuspeitos2);

	//TELA DE MISSÃO
	jogo.fundomissao = new createjs.Bitmap(arquivos.getResult("fundomissao"));
	jogo.fundomissao.on("click", fazNada, this);
	jogo.fundomissao.x = tablet.tela.x + 28;
	jogo.fundomissao.y = tablet.tela.y + 52;
	missao = new createjs.Text("", "16px Oxygen", "#ffffff");
	jogo.menubtn[2].box.addChild(jogo.fundomissao, missao);

	//MANDADO DE PRISÃO
	jogo.fundomandado = new createjs.Bitmap(arquivos.getResult("fundomandado"));
	jogo.fundomandado.on("click", fazNada, this);
	jogo.fundomandado.x = tablet.tela.x + 28;
	jogo.fundomandado.y = tablet.tela.y + 52;

	jogo.fotoMandado = new createjs.Bitmap(arquivos.getResult("desconhecido"));
	jogo.fotoMandado.x = tablet.tela.x + tablet.tela.getBounds().width/2 - jogo.fotoMandado.getBounds().width/2;
	jogo.fotoMandado.y = tablet.tela.y + 325;
	jogo.fotoMandado.num = 1;
	jogo.fotoMandado.on("click", jogo.alteraMandado, this);

	jogo.fotoMandado.proximoSuspeito = new createjs.Bitmap(arquivos.getResult("botaoProximaTela"));
	jogo.fotoMandado.proximoSuspeito.x = jogo.fotoMandado.x + jogo.fotoMandado.getBounds().width + 20;
	jogo.fotoMandado.proximoSuspeito.y = jogo.fotoMandado.y + jogo.fotoMandado.getBounds().height/2 - jogo.fotoMandado.proximoSuspeito.getBounds().height/2;
	jogo.fotoMandado.proximoSuspeito.num = 1;
	jogo.fotoMandado.proximoSuspeito.on("click", jogo.alteraMandado, this);
	jogo.fotoMandado.suspeitoAnterior = new createjs.Bitmap(arquivos.getResult("botaoTelaAnterior"));
	jogo.fotoMandado.suspeitoAnterior.x = jogo.fotoMandado.x - jogo.fotoMandado.suspeitoAnterior.getBounds().width - 20;
	jogo.fotoMandado.suspeitoAnterior.y = jogo.fotoMandado.y + jogo.fotoMandado.getBounds().height/2 - jogo.fotoMandado.suspeitoAnterior.getBounds().height/2;
	jogo.fotoMandado.suspeitoAnterior.num = -1;
	jogo.fotoMandado.suspeitoAnterior.on("click", jogo.alteraMandado, this);

	jogo.nomeMandado = new createjs.Text("Nenhum", "bold 17px Oxygen", "#000000");
	jogo.nomeMandado.textAlign = "center";
	jogo.nomeMandado.x = jogo.fotoMandado.x + jogo.fotoMandado.getBounds().width/2;
	jogo.nomeMandado.y = jogo.fotoMandado.y + jogo.fotoMandado.getBounds().height + 10;

	jogo.descricaoMandado = new createjs.Text("Nenhuma descrição disponível", "15px Oxygen", "#000000");
	jogo.descricaoMandado.lineWidth = 160;
	jogo.descricaoMandado.textAlign = "center";
	jogo.descricaoMandado.x = jogo.fotoMandado.x;
	jogo.descricaoMandado.y = jogo.fotoMandado.y + 30;

	jogo.menubtn[4].box.addChild(jogo.fundomandado, jogo.fotoMandado, jogo.fotoMandado.proximoSuspeito,
		jogo.fotoMandado.suspeitoAnterior, jogo.nomeMandado);

	//OPÇÕES
	jogo.opcao = [];
	jogo.opcao[0] = new createjs.Bitmap(arquivos.getResult("continuar"));
	jogo.opcao[1] = new createjs.Bitmap(arquivos.getResult("recomecar"));
	jogo.opcao[2] = new createjs.Bitmap(arquivos.getResult("som"));
	jogo.opcao[3] = new createjs.Bitmap(arquivos.getResult("voltar"));
	jogo.opcao[4] = new createjs.Bitmap(arquivos.getResult("semSom"));

	jogo.opcao[0].texto = new createjs.Text("Continuar", "35px AgencyFB", "#000000");
	jogo.opcao[1].texto = new createjs.Text("Recomeçar", "35px AgencyFB", "#000000");
	jogo.opcao[2].texto = new createjs.Text("Som", "35px AgencyFB", "#000000");
	jogo.opcao[3].texto = new createjs.Text("Menu Principal", "35px AgencyFB", "#000000");

	jogo.menubtn[5].box = new createjs.Container();
	jogo.fundomenu = new createjs.Bitmap(arquivos.getResult("fundomenu"));
	jogo.fundomenu.on("click", fazNada, this);
	jogo.fundomenu.x = tablet.tela.x + 28;
	jogo.fundomenu.y = tablet.tela.y + 52;

	jogo.menubtn[5].box.addChild(jogo.fundomenu);

	for (var a = 0; a < 4; a++) {
		parametrizar(jogo.opcao[a]);
		jogo.opcao[a].num = a;
		jogo.opcao[a].on("click", jogo.op);
		jogo.opcao[a].x = tablet.tela.x + canvas.height/10;
		jogo.opcao[a].y = tablet.tela.y + canvas.height/6 + a*canvas.height/6 + 36;
		parametrizar(jogo.opcao[a].texto);
		jogo.opcao[a].texto.x = canvas.width*0.425;
		jogo.opcao[a].texto.y = canvas.height*0.19 + a*canvas.height*0.165 + 43;
		jogo.opcao[a].texto.num = a;
		jogo.opcao[a].texto.on("click", jogo.op);
		jogo.menubtn[5].box.addChild(jogo.opcao[a], jogo.opcao[a].texto);
	}

	//Aqui eu estou sobrescrevendo a função de mouse do botão de mute acima
	//A ideia é que sejam criadas funções para cada botão, ao invés de uma só que lide com todos os 4
	jogo.opcao[2].on("click", muteSounds, this);
	jogo.opcao[2].on("mouseover", muteSounds, this);
	jogo.opcao[2].on("mouseout", muteSounds, this);
	jogo.opcao[2].texto.on("click", muteSounds, this);
	jogo.opcao[2].texto.on("mouseover", muteSounds, this);
	jogo.opcao[2].texto.on("mouseout", muteSounds, this);

	jogo.menubtn[0].box.addChild(mapa.comp);
	
	//Tick
	createjs.Ticker.on("tick", tick);
	createjs.Ticker.setFPS(100);

	//Adiciona a extensão para tocar o mp3
	createjs.Sound.alternateExtensions = ["mp3"];

	jogo.trajeto = [];
	jogo.local = [];
	menu();
}

function tick(){
	if(tablet.ativado == true){
		stage.canvas.style.cursor = "none";
		tablet.maoesquerda.x = stage.mouseX - 23;
		tablet.maoesquerda.y = stage.mouseY - 15;
	}else{
		stage.canvas.style.cursor = "default";
	}
	stage.update();
}

function parametrizar (imagem){//AJUSTA AS IMAGENS AO TAMANHO DO STAGE.
	imagem.scaleX = canvas.width/800;
	imagem.scaleY = canvas.width/800;
	imagem.largura = imagem.getBounds().width*imagem.scaleX;
	imagem.altura = imagem.getBounds().height*imagem.scaleY;
}

function ajustar (imagem, x, y, largura, altura){
	imagem.x = x;
	imagem.y = y;
	imagem.scaleX = largura / imagem.getBounds().width;
	imagem.scaleY = altura / imagem.getBounds().height;
}

//MENU INICIAL
menu = function (){
	menu.bg = new createjs.Bitmap(arquivos.getResult("menu"));
	menu.iniciar = new createjs.Bitmap(arquivos.getResult("jogar"));
	menu.iniciar.x = 478.6;
	menu.iniciar.y = 320;
	menu.somBtn = new createjs.Bitmap(arquivos.getResult("somBtn"));
	menu.somBtn.x = 475;
	menu.somBtn.y = menu.iniciar.y + 110;
	menu.ajudaBtn = new createjs.Bitmap(arquivos.getResult("ajudaBtn"));
	menu.ajudaBtn.x = 620;
	menu.ajudaBtn.y = menu.iniciar.y + 110;
	menu.random = new createjs.Bitmap(arquivos.getResult("random"));
	menu.random.x = menu.iniciar.x + 151;
	menu.random.y = menu.iniciar.y + 229;
	menu.fundoCreditos = new createjs.Bitmap(arquivos.getResult("fundoCreditos"));
	menu.fundoCreditos.alpha = 0;
	menu.voltarCreditos = new createjs.Bitmap(arquivos.getResult("botaoTelaAnterior"));
	menu.voltarCreditos.x = 690;
	menu.voltarCreditos.y = 528;
	menu.voltarCreditos.alpha = 0;
	menu.nossoSite = new createjs.Bitmap(arquivos.getResult("nossoSite"));
	menu.nossoSite.x = 370;
	menu.nossoSite.y = 480;
	menu.nossoSite.alpha = 0;
	menu.fundoAjuda = new createjs.Bitmap(arquivos.getResult("fundoAjuda"));
	menu.fundoAjuda.alpha = 0;
	menu.voltarAjuda = new createjs.Bitmap(arquivos.getResult("botaoTelaAnterior"));
	menu.voltarAjuda.x = 690;
	menu.voltarAjuda.y = 528;
	menu.voltarAjuda.alpha = 0;

	parametrizar(menu.bg);
	parametrizar(menu.iniciar);

	stage.addChild(menu.bg, menu.iniciar, menu.somBtn, menu.ajudaBtn, menu.random, menu.fundoCreditos, menu.fundoAjuda, menu.voltarCreditos, menu.voltarAjuda, menu.nossoSite);
	//Adiciona ao botão "investigar" o evento de click para começar o jogo
	menu.iniciar.on("click", mostraAnimacaoInicial);
	menu.iniciar.on("mouseover", jogo.comecar);
	menu.iniciar.on("mouseout", jogo.comecar);

	//Adiciona ao botão "mute" as funções de áudio
	menu.somBtn.on("click", muteSounds, this);
	menu.somBtn.on("mouseover", muteSounds, this);
	menu.somBtn.on("mouseout", muteSounds, this);

	//Adiciona ao botão "mute" as funções de áudio
	menu.ajudaBtn.on("click", ajuda, this);
	menu.ajudaBtn.on("mouseover", ajuda, this);
	menu.ajudaBtn.on("mouseout", ajuda, this);

	//Adiciona ao botão "random" a função de mostrar os créditos do jogo
	menu.random.on("click", creditos, this);
	menu.random.on("mouseover", creditos, this);
	menu.random.on("mouseout", creditos, this);

	//Adiciona ao botão "voltar" a função de voltar para o menu
	menu.voltarCreditos.on("click", creditos, this);
	menu.voltarCreditos.on("mouseover", creditos, this);
	menu.voltarCreditos.on("mouseout", creditos, this);

	//Adiciona ao botão "voltar" a função de voltar para o menu
	menu.voltarAjuda.on("click", ajuda, this);
	menu.voltarAjuda.on("mouseover", ajuda, this);
	menu.voltarAjuda.on("mouseout", ajuda, this);

	//Adiciona à URL do blog os eventos de mouse
	menu.nossoSite.on("click", abreSiteRandom, this);
	menu.nossoSite.on("mouseover", abreSiteRandom, this);
	menu.nossoSite.on("mouseout", abreSiteRandom, this);
}

//FUNÇÃO QUE MOSTRA A TELA DE CRÉDITOS
creditos = function(event){
	if(event.type == "mouseover"){
		highlight(event.target, 1);
	}else if(event.type == "mouseout"){
		highlight(event.target, 0);
	}else{
		if(menu.fundoCreditos.alpha == 0){
			createjs.Tween.get(menu.fundoCreditos).to({alpha: 1}, 1000);
			createjs.Tween.get(menu.voltarCreditos).to({alpha: 1}, 1000);
			createjs.Tween.get(menu.nossoSite).to({alpha: 1}, 1000);
		}else{
			createjs.Tween.get(menu.fundoCreditos).to({alpha: 0}, 1000);
			createjs.Tween.get(menu.voltarCreditos).to({alpha: 0}, 1000);
			createjs.Tween.get(menu.nossoSite).to({alpha: 0}, 1000);
		}
	}
}

//FUNÇÃO QUE MOSTRA A TELA DE AJUDA
ajuda = function(event){
	if(event.type == "mouseover"){
		highlight(event.target, 1);
	}else if(event.type == "mouseout"){
		highlight(event.target, 0);
	}else{
		if(menu.fundoAjuda.alpha == 0){
			createjs.Tween.get(menu.fundoAjuda).to({alpha: 1}, 1000);
			createjs.Tween.get(menu.voltarAjuda).to({alpha: 1}, 1000);
		}else{
			createjs.Tween.get(menu.fundoAjuda).to({alpha: 0}, 1000);
			createjs.Tween.get(menu.voltarAjuda).to({alpha: 0}, 1000);
		}
	}
}

//Animação inicial
function mostraAnimacaoInicial(){
	createjs.Tween.get(menu).to({alpha: 0},1000).call(
		function(){
			stage.removeChild(menu.bg, menu.iniciar, menu.somBtn, menu.ajudaBtn, menu.random);
		});
	createjs.Tween.get(animacaoInicial.tela).to({alpha: 1},1000).call(
		function(){
			createjs.Tween.get(animacaoInicial.tablet.comp).to({y: -587}, 1000).wait(200).call(
				function(){
					createjs.Tween.get(animacaoInicial.caixa).to({alpha: 1}, 200).wait(200).call(proximaFala);
				});
		});

	stage.addChild(animacaoInicial.tela);
}

//Passa para a próxima fala do Canário
function proximaFala(){
	animacaoInicial.setinha.removeAllEventListeners();
	if(animacaoInicial.numFala > 5){
		createjs.Tween.get(animacaoInicial.setinha).to({alpha: 0}, 500);
		createjs.Tween.get(animacaoInicial.texto).to({alpha: 0}, 500);
		createjs.Tween.get(animacaoInicial.caixa).to({alpha: 0}, 500);
		createjs.Tween.get(animacaoInicial.investigar).to({alpha: 1}, 500);
		animacaoInicial.investigar.on("click", jogo.comecar);
	}else{
		animacaoInicial.setinha.visible = true;
		animacaoInicial.texto.text = animacaoInicial.falas[animacaoInicial.numFala];
		animacaoInicial.setinha.on("click", proximaFala);
		animacaoInicial.numFala++;
	}
}

//DA INICIO AO JOGO, DEFININDO TRAJETO E TEMPO.
jogo.comecar = function(evento){
	if(evento.type == "click"){
		if(precisaTutorial){
			mostraTutorial();
		}else{
			tempo = 0;
			alvo = 0;
			jogo.definetrajeto();
			jogo.definelocais();
			jogo.missao();
		}
	}else if(evento.type == "mouseover"){
		//Adiciona uma sombra que funciona como glow filter
		menu.iniciar.shadow = new createjs.Shadow("#ffffff", 0, 0, 4);
	}else{
		//Retira a sombra
		menu.iniciar.shadow = null;
	}
	culpado = Math.floor(Math.random()*4);
}

//DEFINE O TRAJETO DE FORMA ALEATORIA MAS DE QUE OS NUMEROS NÃO SE REPITAM.
jogo.definetrajeto = function(){
	for (var a = 0; a < 5; a++) {
		jogo.trajeto[a] = Math.floor(Math.random() * pais.length);
	}
	var rep=false;
	var b=0;
	while (b < jogo.trajeto.length){
		rep = false;
		for (var c=0;c < jogo.trajeto.length; c++){
			if ((jogo.trajeto[b]==jogo.trajeto[c]) && (b != c)){
				rep = true;
			}
		}
		if (rep != true){
			b++;
		} else {
			jogo.trajeto[b] = Math.floor(Math.random() * pais.length);
		}
	}
	p = jogo.trajeto[0];
	mapa.pin.x = mapa.ponto[p].x - canvas.width*0.019;
	mapa.pin.y = mapa.ponto[p].y - canvas.height*0.04;
	alvo++;
	tempo = 0;
	for (var i = 1;i < jogo.trajeto.length-1; i++){
		tempo += Math.floor(Math.sqrt(Math.pow((mapa.ponto[i].x-mapa.ponto[i+1].x),2) + Math.pow((mapa.ponto[i].y-mapa.ponto[i-1].y),2))*0.125);
	}
}

//DEFINE OS LOCAIS DO TABLET DE FORMA ALEATORIA MAS DE QUE OS NUMEROS NÃO SE REPITAM.
jogo.definelocais = function(){
	jogo.menubtn[1].box.removeAllChildren();
	for (var a = 0; a < 3; a++) {
		jogo.local[a]= Math.floor(Math.random() * (pais[0].local.length));
	}
	var rep=false;
	var b=0;
	while (b < jogo.local.length){
		rep = false;
		for (var c=0;c < jogo.local.length; c++){
			if (((jogo.local[b]==jogo.local[c]) && (b != c)) || (jogo.local[b]==pais[0].local.length) || (jogo.local[b]==0)){
				rep = true;
			}
		}
		if (rep != true){
			b++;
		} else {
			jogo.local[b]= Math.floor(Math.random() * (pais[0].local.length));
		}
	}

	jogo.fundolugares = new createjs.Bitmap(arquivos.getResult("fundolugares"));
	jogo.fundolugares.on("click", fazNada, this);
	jogo.fundolugares.x = tablet.tela.x + 28;
	jogo.fundolugares.y = tablet.tela.y + 52;
	jogo.menubtn[1].box.addChild(jogo.fundolugares);

	local[jogo.local[0]].icone.x = tablet.tela.x + tablet.tela.largura/5;
	local[jogo.local[1]].icone.x = tablet.tela.x + tablet.tela.largura - tablet.tela.largura/5 - local[jogo.local[1]].icone.largura;
	local[jogo.local[2]].icone.x = tablet.tela.x + tablet.tela.largura/5;

	local[jogo.local[0]].icone.y = tablet.tela.y + tablet.tela.altura/3.8;
	local[jogo.local[1]].icone.y = tablet.tela.y + tablet.tela.altura/3.8;
	local[jogo.local[2]].icone.y = tablet.tela.y + tablet.tela.altura - tablet.tela.altura/4.1 - local[jogo.local[1]].icone.altura;

	local[0].icone.x = tablet.tela.x + tablet.tela.largura - tablet.tela.largura/5 - local[jogo.local[1]].icone.largura;
	local[0].icone.y = tablet.tela.y + tablet.tela.altura - tablet.tela.altura/4.1 - local[jogo.local[1]].icone.altura;
	//Adicionando o ponto do aeroporto (tive que fazer assim, infelizmente)
	var pontoAeroporto = new createjs.Bitmap(arquivos.getResult("pontolugares"));
	pontoAeroporto.regX = pontoAeroporto.getBounds().width/2;
	pontoAeroporto.x = local[0].icone.x + local[0].icone.getBounds().width/2;
	pontoAeroporto.y = local[0].icone.y + 15;
	jogo.menubtn[1].box.addChild(local[0].icone, pontoAeroporto);

	for (var i=0; i < jogo.local.length; i++){
		//Adicionando os outros pontos
		var pontoLugar = new createjs.Bitmap(arquivos.getResult("pontolugares"));
		pontoLugar.regX = pontoLugar.getBounds().width/2;
		pontoLugar.x = local[jogo.local[i]].icone.x + local[jogo.local[i]].icone.getBounds().width/2;
		pontoLugar.y = local[jogo.local[i]].icone.y + 15;
		jogo.menubtn[1].box.addChild(local[jogo.local[i]].icone, pontoLugar);
	}
	esconderijo = jogo.local[Math.floor(Math.random()*jogo.local.length)];
}

//TELA QUE EXIBE A PRIMEIRA MISSÃO
jogo.missao = function (event) {
	stage.removeAllChildren();
	l = 0;
	missao.text = pais[jogo.trajeto[0]].missao;
	missao.color = "#ffffff";
	missao.x = canvas.width * 0.32;
	missao.y = canvas.height * 0.58;
	missao.lineWidth = canvas.width * 0.38;
	var iniciar = new createjs.Bitmap(arquivos.getResult("jogar"));
	iniciar.x = canvas.width * 0.57;
	iniciar.y = canvas.height * 0.80;
	iniciar.scaleX = 0.35;
	iniciar.scaleY = 0.35;

	iniciar.on("click", this.investiga);
	tablet.ativado = false;
	stage.addChild(jogo.telamissao, missao, iniciar);
}

//FUNCAO QUE DEFINE O JOGO TODO: QUAL FUNDO DEVE SER MOSTRADO, QUAL PERSONGEM, QUAL CENARIO....

jogo.investiga = function (event){
	//Removendo o texto anterior para não ficar sobreposto
	jogo.menubtn[2].box.removeChild(missao);
	jogo.menubtn[2].box.addChild(missao);//DEFINE A MISSAO DA TELA DE MISSOES DO TABLET
	missao.color = "#000000";
	missao.x = canvas.width * 0.325;
	missao.y = canvas.height * 0.4;
	missao.lineWidth = canvas.width * 0.37;
	stage.removeAllChildren();//REMOVE TUDO DO PALCO
	stage.addChild(cenario[l]);//ADICIONA O CENARIO CORRESPONDENTE

	stage.addChild(pais[p].objeto[l], pais[p].npc[l]);//ADICIONA O PERSOAGEM CORRESPONDENTE AO PAÍS E AO PERSONAGEM

	//Muda o texto do tempo restante
	jogo.tempoRestante.text = tempo+":00";//EXIBE O TEMPO RESTANTE

	var nomeLugar = new createjs.Text(local[l].nome.toUpperCase(), "27px AgencyFB", "#ffffff");//NOME DO LOCAL
	nomeLugar.x = jogo.caixaLocalizacao.x + 16;
	nomeLugar.y = jogo.caixaLocalizacao.y + 37;

	jogo.localizacao.removeChildAt(2);
	jogo.localizacao.removeChildAt(1);
	jogo.localizacao.addChild(pais[p].titulo, nomeLugar);

	//PREENCHE A CAIXA DE DIALOGO
	var info = new createjs.Text("Info", "16px Oxygen", "#ffffff");
	info.lineHeight = 22;

	//Se o cenário for o do aeroporto, a caixa de diálogo é diferente
	if(l == 0){
		jogo.dialogo.image = arquivos.getResult("caixa-dialogo-aeroporto");
		jogo.dialogo.x = 308;
		jogo.dialogo.y = 448;
		info.lineWidth = 340;
	}else{
		jogo.dialogo.image = arquivos.getResult("dialogo");
		jogo.dialogo.x = 102;
		jogo.dialogo.y = 448;
		info.lineWidth = 520;
	}

	//Agora, posiciona-se o texto de acordo com a caixa de diálogo...
	info.x = jogo.dialogo.x + 25;
	info.y = jogo.dialogo.y + 51;

	//... e também o nome do personagem que está na tela
	pais[p].npc[l].nome.x = jogo.dialogo.x + 25;
	pais[p].npc[l].nome.y = jogo.dialogo.y + 9;

	//Aqui é para colocar o pino nos lugares dentro do país
	var pinlugar = new createjs.Bitmap(arquivos.getResult("pinlugar"));
	pinlugar.regX = pinlugar.getBounds().width/2;
	pinlugar.x = local[l].icone.x + local[l].icone.getBounds().width/2;
	pinlugar.y = local[l].icone.y -5;
	jogo.menubtn[1].box.addChild(pinlugar);

	//ADICIONA OS BOTOES AO PALCO
	for (var i = 0; i <6; i++){
		jogo.menubtn[i].removeAllEventListeners();
		jogo.menubtn[i].on("mouseover", mostraLegenda);
		jogo.menubtn[i].on("mouseout", mostraLegenda);
		jogo.menubtn[i].on("click", tela);
	}
	//ADICIONA BOTOES, DIALOGO, E BRASAO.
	stage.addChild(jogo.perfil, jogo.brasaoAtual, jogo.localizacao, pais[p].icone, jogo.dialogo, jogo.menu, info, pais[p].npc[l].nome);

	//E se o cenário for o do aeroporto, preciso colocar a placa com as informações do país
	if(l == 0){
		pais[p].informacao.x = 30;
		pais[p].informacao.y = 147;
		stage.addChild(pais[p].informacao);
	}

	if (tempo <= 0){
		final(0);
	}
	//DEFINE O TEXTO DA CAIXA DE DIALOGO.
	if (p==jogo.trajeto[jogo.trajeto.length - 1] && alvo >= jogo.trajeto.length) {
		info.text = "Algo estranho está acontecendo na cidade.";
			if (l == esconderijo){
				if(suspeitoMandado == culpado){
					final(2);
				}else{
					final(1);
				}
			}
	} else {
		if (l == 0){
			info.text = pais[p].info;
		} else {
			if (p == jogo.trajeto[alvo-1]){
				var aff = Math.round(Math.random());
				console.log(aff);
				info.text = pais[jogo.trajeto[alvo]].local[l].dica[aff] + " " + suspeitos[culpado].caracteristica[Math.floor(Math.random()*suspeitos[culpado].caracteristica.length)];
			} else {
				info.text = "Não me lembro de ver esta pessoa por aqui.";
			}
		}
	}
}
//DESATIVA O TABLET
tablet.ativado = false;

//ATIVA O TABLET E O PREENCHE.
tablet.ativar = function (){
	filtro = new createjs.Shape();
	for (var i = 0; i <6; i++){
		jogo.menubtn[i].removeAllEventListeners();
		jogo.menubtn[i].shadow = null;
	}
	filtro.graphics.beginFill("rgba(255, 255, 255, 0.5)").drawRect(0, 0, canvas.width, canvas.height);
	stage.addChild(filtro, tablet.comp, tablet.maoesquerda);
	filtro.on("click", tablet.desativar);
	tablet.ativado=true;
	createjs.Tween.get(tablet.comp).to({y:13}, 500);
}
//REMOVE O TABLET DA TELA
tablet.desativar = function(evento){
	for (var i = 0; i <6; i++){
		jogo.menubtn[i].on("click", tela);
		jogo.menubtn[i].on("mouseover", mostraLegenda);
		jogo.menubtn[i].on("mouseout", mostraLegenda);
	}
	stage.removeChild(filtro);
	stage.removeChild(tablet.maoesquerda);
	tablet.ativado = false;
	createjs.Tween.get(tablet.comp).to({y:canvas.height}, 500);
}
//CONTROLA O TABLET
function tela (evento){
	switch(evento.type){
		case "click":
			for (var i = 0; i <6; i++){
				tablet.comp.removeChild(jogo.menubtn[i].box);
				jogo.legendabtn[i].visible = false;
			}
			tablet.comp.removeChild(jogo.brasaoAtual.box);
			if (tablet.ativado==false){
				tablet.ativar();
				tablet.comp.addChild(evento.target.box, tablet.maodireita);
			} else {
				tablet.desativar();
			}
			break;
		case "mouseover":
			highlight(evento.target, 1);
			break;
		case "mouseout":
			highlight(evento.target, 0);
			break;
	}
}
//MOSTRA A LEGENDA DOS BOTOES.
function mostraLegenda(event){
	if(event.type == "mouseover"){
		jogo.legendabtn[event.target.num].visible = true;
		event.target.shadow = new createjs.Shadow("#ffffff", 0, 0, 3);
	}
	else if(event.type == "mouseout"){
		jogo.legendabtn[event.target.num].visible = false;
		event.target.shadow = null;
	}
}
//CONTROLA O PAIS ATUAL E O PROXIMO PAIS DO TRAJETO
jogo.pais = function (event){
	l = 0;
	tempo -= Math.floor(Math.sqrt(Math.pow((mapa.ponto[p].x-mapa.ponto[event.target.pais].x),2) + Math.pow((mapa.ponto[p].y-mapa.ponto[event.target.pais].y),2))*0.025);
	p = event.target.pais;
	if (p == jogo.trajeto[alvo]){
		alvo ++;
	}
	mapa.pin.x = mapa.ponto[p].x - canvas.width*0.019;
	mapa.pin.y = mapa.ponto[p].y - canvas.height*0.04;
	jogo.definelocais();
	tablet.desativar();
	jogo.investiga();
}
//DEFINE QUAL O LOCAL DENTRO DO PAÍS A SER CARREGADO.
jogo.locais = function (event){
	l = event.target.local;
	tempo--;
	//Essa linha aqui serve pra remover o pino do local que você está, antes de trocar de posição
	jogo.menubtn[1].box.removeChildAt(jogo.menubtn[1].box.numChildren-1);
	tablet.desativar();
	jogo.investiga();
}

//MOSTRA FOTO DOS SUSPEITOS
function mostraFotoGrande(event){
	var alvo = event.target.num;
	if(suspeitos[alvo].fotoGrande.scaleX == 0){
		//Esse for checa se já tem alguma foto grande sendo mostrada
		for(var i=0; i<5; i++){
			if(suspeitos[i].fotoGrande.scaleX == 1){
				createjs.Tween.get(suspeitos[i].fotoGrande).to({
					x: suspeitos[i].fotoPequena.x + suspeitos[i].fotoPequena.getBounds().width/2,
					y: suspeitos[i].fotoPequena.y + suspeitos[i].fotoPequena.getBounds().height/2,
					scaleX: 0,
					scaleY: 0
				}, 200);
			}
		}

		createjs.Tween.get(suspeitos[alvo].fotoGrande).to({
			x: tablet.tela.x + 135,
			y: suspeitos[0].fotoPequena.y,
			scaleX: 1,
			scaleY: 1
		}, 200);
	}else{
		createjs.Tween.get(suspeitos[alvo].fotoGrande).to({
			x: suspeitos[alvo].fotoPequena.x + suspeitos[alvo].fotoPequena.getBounds().width/2,
			y: suspeitos[alvo].fotoPequena.y + suspeitos[alvo].fotoPequena.getBounds().height/2,
			scaleX: 0,
			scaleY: 0
		}, 200);
	}
}

//ALTERA O MANDADO
jogo.alteraMandado = function(event){
	suspeitoMandado += event.target.num;

	if(suspeitoMandado > suspeitos.length-1){
		suspeitoMandado = 0;
	}else if(suspeitoMandado < 0){
		suspeitoMandado = suspeitos.length-1;
	}

	jogo.fotoMandado.image = suspeitos[suspeitoMandado].fotoPequena.image;
	jogo.nomeMandado.text = suspeitos[suspeitoMandado].nome;
}
//GANHA OS BRASOES
jogo.ganhaMedalha = function(){
	var contador = 0;
	for(var i=0; i<jogo.medalhasConquistadas.length; i++){
		if(jogo.medalhasConquistadas[i] == true){
			contador++;
		}
	}

	if(contador >= jogo.medalhasConquistadas.length){
		jogo.ganhaBrasao();
	}
	else{
		jogo.medalhasConquistadas[contador] = true;
		jogo.medalhas[contador].image = arquivos.getResult("medalhaColorida");
	}
}

jogo.ganhaBrasao = function(){
	var contador = 0;
	//Zera as medalhas
	for(var i=0; i<jogo.medalhasConquistadas.length; i++){
		jogo.medalhas[i].image = arquivos.getResult("medalhaCinza");
		jogo.medalhasConquistadas[i] = false;
	}

	//Aumenta o brasão
	for(var i=0; i<jogo.brasoesConquistados.length; i++){
		if(!(jogo.brasoesConquistados[i].image == arquivos.getResult("nenhum"))){
			contador++;
		}
	}

	if(contador < jogo.brasoesConquistados.length){
		jogo.brasoesConquistados[contador].image = jogo.brasoes[contador].image;
		jogo.brasoesConquistados[contador].grande.image = jogo.brasoes[contador].grande.image;
		jogo.brasaoAtual.image = jogo.brasoesConquistados[contador].image;
	}
}

//CONTROLE O TABLET
jogo.op = function(event){
	switch (event.target.num) {
    case 0:
    	tablet.desativar();
        break;
	case 1:
		jogo.comecar();
		break;
	case 3:
		stage.removeAllChildren();
		tablet.desativar();
		menu();
	}
}

//MOSTRA A LOCALIZACAO
jogo.mostraLocalizacao = function(event){
	if(event.type == "mouseover"){
		jogo.localizacao.visible = true;
		event.target.shadow = new createjs.Shadow("#ffffff", 0, 0, 1);
	}
	else if(event.type == "mouseout"){
		jogo.localizacao.visible = false;
		event.target.shadow = null;
	}
}

//CARREGA O JOGO
function loadBar(event){
	barraProgresso.scaleX = arquivos.progress;
	textoBarraProgresso.text = Math.round(arquivos.progress*100)+"%";
	console.log(textoBarraProgresso.text);
	stage.update();
}

//DEFINE A ULTIMA TELA
function final(opcao){
	switch (opcao){
		case 0:
			stage.removeAllChildren();
			animacaoInicial.canarioFeliz.visible = false;
			animacaoInicial.canarioRaiva.visible = true;
			animacaoInicial.canario.visible = false;
			animacaoInicial.tablet.comp.y = 0;
			stage.addChild(animacaoInicial.tela);

			animacaoInicial.investigar.alpha = 0;
			stage.addChild(animacaoInicial.tela);

			createjs.Tween.get(animacaoInicial.tablet.comp).wait(1000).to({y: -587}, 1000).call(
				function(){
					createjs.Tween.get(animacaoInicial.caixa).to({alpha: 1}, 200).call(
						function(){
							animacaoInicial.texto.alpha = 1;
							animacaoInicial.setinha.alpha = 1;
							animacaoInicial.setinha.on("click", proximaFala);
							animacaoInicial.texto.text = "Você foi muito lento e o suspeito acabou fugindo da América Latina... Mas já temos uma nova missão.\nEstá pronto?";
						});
				});
		break;
		case 1:
			stage.removeAllChildren();
			animacaoInicial.canarioFeliz.visible = false;
			animacaoInicial.canarioRaiva.visible = true;
			animacaoInicial.canario.visible = false;
			animacaoInicial.suspeitoFinal.image = suspeitos[culpado].fotoFinal.image;
			animacaoInicial.tablet.comp.y = 0;
			stage.addChild(animacaoInicial.tela);

			animacaoInicial.investigar.alpha = 0;
			stage.addChild(animacaoInicial.tela);

			createjs.Tween.get(animacaoInicial.tablet.comp).wait(1000).to({y: -587}, 1000).call(
				function(){
					createjs.Tween.get(animacaoInicial.caixa).to({alpha: 1}, 200).call(
						function(){
							animacaoInicial.texto.alpha = 1;
							animacaoInicial.setinha.alpha = 1;
							animacaoInicial.setinha.on("click", proximaFala);
							animacaoInicial.texto.text = "Seu mandado aponta para outra pessoa. Não podemos prender esse suspeito!";
						});
				});
		break;
		case 2:
			stage.removeAllChildren();
			animacaoInicial.canarioFeliz.visible = true;
			animacaoInicial.canarioRaiva.visible = false;
			animacaoInicial.canario.visible = false;
			animacaoInicial.suspeitoFinal.image = suspeitos[culpado].fotoFinal.image;
			animacaoInicial.tablet.comp.y = 0;
			animacaoInicial.investigar.alpha = 0;
			stage.addChild(animacaoInicial.tela);

			jogo.ganhaMedalha();

			createjs.Tween.get(animacaoInicial.tablet.comp).wait(1000).to({y: -587}, 1000).call(
				function(){
					createjs.Tween.get(animacaoInicial.caixa).to({alpha: 1}, 200).call(
						function(){
							animacaoInicial.texto.alpha = 1;
							animacaoInicial.setinha.alpha = 1;
							animacaoInicial.setinha.on("click", proximaFala);
							animacaoInicial.texto.text = "Parabéns, você chegou a tempo e pegou o criminoso! Preparado para a próxima missão?";
						});
				});
		break;
	}
}

jogo.mudaTelaSuspeitos = function(evento){
	if(evento.target.num == 0){
		jogo.telaSuspeitos1.visible = false;
		jogo.telaSuspeitos2.visible = true;
	}else{
		jogo.telaSuspeitos2.visible = false;
		jogo.telaSuspeitos1.visible = true;
	}
}

primeiraLetraMaiuscula = function(frase){
	return frase.charAt(0).toUpperCase() + frase.slice(1);
}

highlight = function(alvo, estado){
	if(estado != 0){
		alvo.shadow = new createjs.Shadow("#ffffff", 0, 0, 3);
	}else{
		alvo.shadow = null;
	}
}

muteSounds = function(evento){
	switch(evento.type){
		case "click":
			if(createjs.Sound.getVolume() == 0){
				createjs.Sound.setVolume(1);
			}else{
				createjs.Sound.setVolume(0);
			}
			break;
		case "mouseover":
			highlight(evento.target, 1);
			break;
		case "mouseout":
			highlight(evento.target, 0);
			break;
	}
}

//NOVAS FUNÇÕES
mostraTutorial = function(evento){
	if(evento == undefined){
		tutorial.removeAllChildren();
		stage.removeChild(tutorial);
		tutorial.addChild(cenasTutorial[tutorial.indice]);
		stage.addChild(tutorial);
	}else{
		switch(evento.type){
			case "mouseover":
				highlight(evento.target, 1);
				break;
			case "mouseout":
				highlight(evento.target, 0);
				break;
			case "click":
				//Se o atributo "num" do botão indicar o final do vetor "cenasTutorial", então...
				if(evento.target.num >= cenasTutorial.length){
					precisaTutorial = false;
					stage.removeChild(tutorial);
					jogo.comecar();
				}
				//Senão...
				else{
					tutorial.indice = evento.target.num;
					tutorial.removeAllChildren();
					stage.removeChild(tutorial);
					tutorial.addChild(cenasTutorial[tutorial.indice]);
					stage.addChild(tutorial);
				}
				break;
		}
	}
}

//Essa função é para resolver o problema de o tablet ser baixado
//quando o jogador clica numa área que não tem botão nenhum (solução da Julianne)
function fazNada(evento){
}

function abreSiteRandom(evento){
	switch(evento.type){
		case "click":
			window.open("http://www.randomsmd.blogspot.com");
			break;
		case "mouseover":
			highlight(evento.target, 1);
			break;
		case "mouseout":
			highlight(evento.target, 0);
			break;
	}
}