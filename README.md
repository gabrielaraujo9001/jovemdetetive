Versão 1.2

Esse repositório tem o intuito de armazenar todo o nosso projeto do segundo semestre do curso Sistemas e Mídias Digitais da UFC. Somos a equipe Random.

Descrição do projeto: Jovem Detetive é um jogo WEB que se assemelha a "Where in the World is Carmen Sandiego?". A ideia surgiu a partir de uma necessidade do Colégio Sapiens. Todo ano, a escola realiza um evento sobre a América Latina, e este ano eles tiveram a ideia de criar um aplicativo ou jogo que pudesse ser usado durante ou após o evento para testar os conhecimentos dos alunos a respeito da América Latina.

Membros:
- Breno Gomes de Sousa
- Bruno Oliveira Pinheiro
- Francisca Julianne Pessoa Holanda
- Gabriel Rodrigues de Araujo
- Hiago Bruno Rabelo da Silva