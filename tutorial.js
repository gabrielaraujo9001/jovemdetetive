//O Container das cenas do tutorial
var tutorial = new createjs.Container();
//O índice da cena atual no palco
tutorial.indice = 0;
//O vetor que guarda as cenas do tutorial
var cenasTutorial = [];

//Função que será chamada quando todas as imagens forem carregadas
//Não queremos que dê problema pelo fato de este arquivo
//ser chamado ao mesmo tempo que o arquivo principal
criaCenasTutorial = function(){
	//Cena 1 -----------------------------------
	cenasTutorial[0] = new createjs.Container();
	cenasTutorial[0].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial01"));
	cenasTutorial[0].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[0].proximo.x = 543;
	cenasTutorial[0].proximo.y = 365;
	cenasTutorial[0].proximo.num = 1;
	cenasTutorial[0].proximo.on("click", mostraTutorial, this);
	cenasTutorial[0].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[0].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[0].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[0].pularTutorial.x = 710;
	cenasTutorial[0].pularTutorial.y = 515;
	cenasTutorial[0].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[0].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[0].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[0].addChild(cenasTutorial[0].fundo, cenasTutorial[0].proximo, cenasTutorial[0].pularTutorial);

	//Cena 2 -----------------------------------
	cenasTutorial[1] = new createjs.Container();
	cenasTutorial[1].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial02"));
	cenasTutorial[1].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[1].proximo.x = 543;
	cenasTutorial[1].proximo.y = 365;
	cenasTutorial[1].proximo.num = 2;
	cenasTutorial[1].proximo.on("click", mostraTutorial, this);
	cenasTutorial[1].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[1].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[1].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[1].pularTutorial.x = 710;
	cenasTutorial[1].pularTutorial.y = 515;
	cenasTutorial[1].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[1].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[1].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[1].addChild(cenasTutorial[1].fundo, cenasTutorial[1].proximo, cenasTutorial[1].pularTutorial);

	//Cena 2.2 -----------------------------------
	cenasTutorial[2] = new createjs.Container();
	cenasTutorial[2].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial03"));
	cenasTutorial[2].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[2].proximo.x = 543;
	cenasTutorial[2].proximo.y = 365;
	cenasTutorial[2].proximo.num = 3;
	cenasTutorial[2].proximo.on("click", mostraTutorial, this);
	cenasTutorial[2].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[2].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[2].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[2].pularTutorial.x = 710;
	cenasTutorial[2].pularTutorial.y = 515;
	cenasTutorial[2].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[2].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[2].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[2].addChild(cenasTutorial[2].fundo, cenasTutorial[2].proximo, cenasTutorial[2].pularTutorial);

	//Cena 3 -----------------------------------
	cenasTutorial[3] = new createjs.Container();
	cenasTutorial[3].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial04"));
	cenasTutorial[3].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[3].proximo.x = 543;
	cenasTutorial[3].proximo.y = 365;
	cenasTutorial[3].proximo.num = 4;
	cenasTutorial[3].proximo.on("click", mostraTutorial, this);
	cenasTutorial[3].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[3].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[3].pais = new createjs.Bitmap(arquivos.getResult("iconeArgentina"));
	cenasTutorial[3].pais.x = 705;
	cenasTutorial[3].pais.y = 25;
	cenasTutorial[3].pais.on("mouseover", jogo.mostraLocalizacao, this);
	cenasTutorial[3].pais.on("mouseout", jogo.mostraLocalizacao, this);
	cenasTutorial[3].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[3].pularTutorial.x = 710;
	cenasTutorial[3].pularTutorial.y = 515;
	cenasTutorial[3].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[3].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[3].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[3].addChild(cenasTutorial[3].fundo, cenasTutorial[3].proximo, cenasTutorial[3].pularTutorial, cenasTutorial[3].pais);

	//Cena 4 -----------------------------------
	cenasTutorial[4] = new createjs.Container();
	cenasTutorial[4].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial05"));
	cenasTutorial[4].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[4].proximo.x = 543;
	cenasTutorial[4].proximo.y = 365;
	cenasTutorial[4].proximo.num = 5;
	cenasTutorial[4].proximo.on("click", mostraTutorial, this);
	cenasTutorial[4].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[4].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[4].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[4].pularTutorial.x = 710;
	cenasTutorial[4].pularTutorial.y = 515;
	cenasTutorial[4].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[4].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[4].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[4].addChild(cenasTutorial[4].fundo, cenasTutorial[4].proximo, cenasTutorial[4].pularTutorial);

	//Cena 5 -----------------------------------
	cenasTutorial[5] = new createjs.Container();
	cenasTutorial[5].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial06"));
	cenasTutorial[5].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[5].proximo.x = 543;
	cenasTutorial[5].proximo.y = 365;
	cenasTutorial[5].proximo.num = 6;
	cenasTutorial[5].proximo.on("click", mostraTutorial, this);
	cenasTutorial[5].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[5].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[5].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[5].pularTutorial.x = 710;
	cenasTutorial[5].pularTutorial.y = 515;
	cenasTutorial[5].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[5].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[5].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[5].addChild(cenasTutorial[5].fundo, cenasTutorial[5].proximo, cenasTutorial[5].pularTutorial);

	//Cena 6 -----------------------------------
	cenasTutorial[6] = new createjs.Container();
	cenasTutorial[6].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial07"));
	cenasTutorial[6].proximo = new createjs.Bitmap(arquivos.getResult("brasilTutorial"));
	cenasTutorial[6].proximo.x = -7;
	cenasTutorial[6].proximo.y = 0;
	cenasTutorial[6].proximo.num = 7;
	cenasTutorial[6].proximo.on("click", mostraTutorial, this);
	cenasTutorial[6].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[6].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[6].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[6].pularTutorial.x = 710;
	cenasTutorial[6].pularTutorial.y = 515;
	cenasTutorial[6].addChild(cenasTutorial[6].fundo, cenasTutorial[6].proximo, cenasTutorial[6].pularTutorial);

	//Cena 7 -----------------------------------
	cenasTutorial[7] = new createjs.Container();
	cenasTutorial[7].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial08"));
	cenasTutorial[7].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[7].proximo.x = 543;
	cenasTutorial[7].proximo.y = 365;
	cenasTutorial[7].proximo.num = 8;
	cenasTutorial[7].proximo.on("click", mostraTutorial, this);
	cenasTutorial[7].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[7].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[7].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[7].pularTutorial.x = 710;
	cenasTutorial[7].pularTutorial.y = 515;
	cenasTutorial[7].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[7].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[7].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[7].addChild(cenasTutorial[7].fundo, cenasTutorial[7].proximo, cenasTutorial[7].pularTutorial);

	//Cena 8 -----------------------------------
	cenasTutorial[8] = new createjs.Container();
	cenasTutorial[8].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial09"));
	cenasTutorial[8].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[8].proximo.x = 543;
	cenasTutorial[8].proximo.y = 365;
	cenasTutorial[8].proximo.num = 9;
	cenasTutorial[8].proximo.on("click", mostraTutorial, this);
	cenasTutorial[8].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[8].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[8].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[8].pularTutorial.x = 710;
	cenasTutorial[8].pularTutorial.y = 515;
	cenasTutorial[8].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[8].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[8].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[8].addChild(cenasTutorial[8].fundo, cenasTutorial[8].proximo, cenasTutorial[8].pularTutorial);

	//Cena 9 -----------------------------------
	cenasTutorial[9] = new createjs.Container();
	cenasTutorial[9].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial10"));
	cenasTutorial[9].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[9].proximo.x = 543;
	cenasTutorial[9].proximo.y = 365;
	cenasTutorial[9].proximo.num = 10;
	cenasTutorial[9].proximo.on("click", mostraTutorial, this);
	cenasTutorial[9].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[9].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[9].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[9].pularTutorial.x = 710;
	cenasTutorial[9].pularTutorial.y = 515;
	cenasTutorial[9].addChild(cenasTutorial[9].fundo, cenasTutorial[9].proximo, cenasTutorial[9].pularTutorial);

	//Cena 10 -----------------------------------
	cenasTutorial[10] = new createjs.Container();
	cenasTutorial[10].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial11"));
	cenasTutorial[10].proximo = new createjs.Bitmap(arquivos.getResult("bibliotecaTutorial"));
	cenasTutorial[10].proximo.x = 278.2;
	cenasTutorial[10].proximo.y = 151.8;
	cenasTutorial[10].proximo.num = 11;
	cenasTutorial[10].proximo.on("click", mostraTutorial, this);
	cenasTutorial[10].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[10].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[10].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[10].pularTutorial.x = 710;
	cenasTutorial[10].pularTutorial.y = 515;
	cenasTutorial[10].addChild(cenasTutorial[10].fundo, cenasTutorial[10].proximo, cenasTutorial[10].pularTutorial);

	//Cena 11 -----------------------------------
	cenasTutorial[11] = new createjs.Container();
	cenasTutorial[11].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial12"));
	cenasTutorial[11].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[11].proximo.x = 543;
	cenasTutorial[11].proximo.y = 335;
	cenasTutorial[11].proximo.num = 12;
	cenasTutorial[11].proximo.on("click", mostraTutorial, this);
	cenasTutorial[11].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[11].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[11].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[11].pularTutorial.x = 710;
	cenasTutorial[11].pularTutorial.y = 515;
	cenasTutorial[11].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[11].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[11].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[11].addChild(cenasTutorial[11].fundo, cenasTutorial[11].proximo, cenasTutorial[11].pularTutorial);

	//Cena 12 -----------------------------------
	cenasTutorial[12] = new createjs.Container();
	cenasTutorial[12].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial13"));
	cenasTutorial[12].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[12].proximo.x = 543;
	cenasTutorial[12].proximo.y = 335;
	cenasTutorial[12].proximo.num = 13;
	cenasTutorial[12].proximo.on("click", mostraTutorial, this);
	cenasTutorial[12].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[12].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[12].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[12].pularTutorial.x = 710;
	cenasTutorial[12].pularTutorial.y = 515;
	cenasTutorial[12].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[12].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[12].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[12].addChild(cenasTutorial[12].fundo, cenasTutorial[12].proximo, cenasTutorial[12].pularTutorial);

	//Cena 13 -----------------------------------
	cenasTutorial[13] = new createjs.Container();
	cenasTutorial[13].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial14"));
	cenasTutorial[13].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[13].proximo.x = 543;
	cenasTutorial[13].proximo.y = 335;
	cenasTutorial[13].proximo.num = 14;
	cenasTutorial[13].proximo.on("click", mostraTutorial, this);
	cenasTutorial[13].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[13].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[13].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[13].pularTutorial.x = 710;
	cenasTutorial[13].pularTutorial.y = 515;
	cenasTutorial[13].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[13].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[13].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[13].addChild(cenasTutorial[13].fundo, cenasTutorial[13].proximo, cenasTutorial[13].pularTutorial);

	//Cena 14 -----------------------------------
	cenasTutorial[14] = new createjs.Container();
	cenasTutorial[14].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial15"));
	cenasTutorial[14].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[14].proximo.x = 543;
	cenasTutorial[14].proximo.y = 335;
	cenasTutorial[14].proximo.num = 15;
	cenasTutorial[14].proximo.on("click", mostraTutorial, this);
	cenasTutorial[14].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[14].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[14].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[14].pularTutorial.x = 710;
	cenasTutorial[14].pularTutorial.y = 515;
	cenasTutorial[14].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[14].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[14].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[14].addChild(cenasTutorial[14].fundo, cenasTutorial[14].proximo, cenasTutorial[14].pularTutorial);

	//Cena 15 -----------------------------------
	cenasTutorial[15] = new createjs.Container();
	cenasTutorial[15].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial16"));
	cenasTutorial[15].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[15].proximo.x = 543;
	cenasTutorial[15].proximo.y = 335;
	cenasTutorial[15].proximo.num = 16;
	cenasTutorial[15].proximo.on("click", mostraTutorial, this);
	cenasTutorial[15].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[15].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[15].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[15].pularTutorial.x = 710;
	cenasTutorial[15].pularTutorial.y = 515;
	cenasTutorial[15].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[15].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[15].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[15].addChild(cenasTutorial[15].fundo, cenasTutorial[15].proximo, cenasTutorial[15].pularTutorial);

	//Cena 16 -----------------------------------
	cenasTutorial[16] = new createjs.Container();
	cenasTutorial[16].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial17"));
	cenasTutorial[16].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[16].proximo.x = 543;
	cenasTutorial[16].proximo.y = 335;
	cenasTutorial[16].proximo.num = 17;
	cenasTutorial[16].proximo.on("click", mostraTutorial, this);
	cenasTutorial[16].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[16].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[16].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[16].pularTutorial.x = 710;
	cenasTutorial[16].pularTutorial.y = 515;
	cenasTutorial[16].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[16].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[16].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[16].addChild(cenasTutorial[16].fundo, cenasTutorial[16].proximo, cenasTutorial[16].pularTutorial);

	//Cena 17 -----------------------------------
	cenasTutorial[17] = new createjs.Container();
	cenasTutorial[17].fundo = new createjs.Bitmap(arquivos.getResult("fundoTutorial18"));
	cenasTutorial[17].proximo = new createjs.Bitmap(arquivos.getResult("proximoTutorial"));
	cenasTutorial[17].proximo.x = 543;
	cenasTutorial[17].proximo.y = 335;
	cenasTutorial[17].proximo.num = 18;
	cenasTutorial[17].proximo.on("click", mostraTutorial, this);
	cenasTutorial[17].proximo.on("mouseover", mostraTutorial, this);
	cenasTutorial[17].proximo.on("mouseout", mostraTutorial, this);
	cenasTutorial[17].pularTutorial = new createjs.Bitmap(arquivos.getResult("pularTutorial"));
	cenasTutorial[17].pularTutorial.x = 710;
	cenasTutorial[17].pularTutorial.y = 515;
	cenasTutorial[17].pularTutorial.on("click", mostraTutorial, this);
	cenasTutorial[17].pularTutorial.on("mouseover", mostraTutorial, this);
	cenasTutorial[17].pularTutorial.on("mouseout", mostraTutorial, this);
	cenasTutorial[17].addChild(cenasTutorial[17].fundo, cenasTutorial[17].proximo, cenasTutorial[17].pularTutorial);

	//Este é um loop para fazer com que todos os botões "pularTutorial" fiquem
	//com a mesma propriedade .num (o tamanho do vetor-1)
	for(var i=0; i<cenasTutorial.length; i++){
		cenasTutorial[i].pularTutorial.num = cenasTutorial.length;
	}
}