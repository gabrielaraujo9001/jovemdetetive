pais=[];

function paises(nome){
	this.nome=nome;
	this.local=[];
	this.missao=[];
	this.local[1]=new locais("Loja de Discos");
	this.local[2]=new locais("Museu");
	this.local[3]=new locais("Mercado");
	this.local[4]=new locais("Biblioteca");
	this.npc = [];
	this.objeto = [];
	this.container = [];
	this.container[0] = new createjs.Container();
	this.container[1] = new createjs.Container();
	this.container[2] = new createjs.Container();
	this.container[3] = new createjs.Container();
}

function locais(nome){
	this.nome=nome;
	this.dica=[];
}

pais[0] = new paises("México");
pais[0].capital = "Cid. México";
pais[0].info = "Seja muito bem-vindo ao México! Aproveite sua estadia conhecendo os lugares da cidade. Garanto que não vai ficar entediado!";
pais[0].missao[0] = "O Anjo da independência é um monumento localizado no Paseo de la Reforma, no centro da Cidade do México.Foi erguido em 1910 em comemoração ao centenário da Guerra da Independência do México. Siga o responsável pelo crime até seu esconderijo e o prenda.";
pais[0].local[4].dica[0]= "Sim, vi essa pessoa. Ela estava se exibindo porque ia viajar para a maior nação de língua espanhola do mundo... Ah, e ela possuía";
pais[0].local[1].dica[0]= "A pessoa que você procura disse que gostava de música ranchera, que são canções que se preocupam com temas tradicionais do patriotismo, amor e natureza. Ela tinha";
pais[0].local[2].dica[0]= "Eu vi essa pessoa sim. Ela estava em busca de esculturas maias. Se não me engano, ela tinha";
pais[0].local[3].dica[0]= "Sim, eu vi essa pessoa. Ela tinha um gosto bem peculiar. Estava querendo comprar bastante pimenta. Acho que ela tinha";
pais[0].local[4].dica[1]= "Essa pessoa veio aqui para saber mais sobre a Basílica de Nossa Senhora de Guadalupe. Ela possuía";
pais[0].local[1].dica[1]= "Esta pessoa estava à procura de mariachis, grupos que cantam, por exemplo, boleros, um estilo que inclui a música “Solamente Una Vez”. Acho que ela tinha";
pais[0].local[2].dica[1]= "Este sujeito passou por aqui carregando uma bandeira com as cores verde, branco e vermelho. Acho que ele possuía";
pais[0].local[3].dica[1]= "Esse sujeito veio aqui procurando um pouco de guacamole… Muito esquisito, não acha? Ele tinha";

pais[1] = new paises("Venezuela");
pais[1].capital = "Caracas";
pais[1].info = "Seja muito bem-vindo à Venezuela! Aproveite sua estadia conhecendo os lugares da cidade. Garanto que não vai ficar entediado!";
pais[1].missao[0] = "As Torres Gêmeas de Caracas foram roubadas. As torres são dois arranha-céus com 225 metros de altura cada. Juntas, elas se tornaram um importante polo de desenvolvimento para a cidade. Siga o bandido até o seu esconderijo e as recupere.";
pais[1].local[4].dica[0]= "Esse sujeito buscava informações sobre o país com maior número de vencedoras do concurso Miss Universo. Não sei bem a razão disso… Ah, ele tinha";
pais[1].local[1].dica[0]= "Esse sujeito queria comprar discos do cantor e compositor D. Simón Díaz, compositor da famosa música “Caballo Viejo”. Lembro que ele tinha";
pais[1].local[2].dica[0]= "Este sujeito passou por aqui carregando uma bandeira com as cores vermelho, azul e amarelo. Ah, ele tinha";
pais[1].local[3].dica[0]= "Esse sujeito veio em busca de uma comida típica chamada… pabellón criollo, se não me engano. Ela possuía";
pais[1].local[4].dica[1]= "Esse sujeito estava aqui pesquisando sobre um país cujo principal esporte é o beisebol. Ele tinha";
pais[1].local[1].dica[1]= "Essa pessoa disse que iria participar de um baile típico chamado Joropo. Deve ser uma festa bem especial. Ela possuía";
pais[1].local[2].dica[1]= "Essa pessoa veio aqui sim. Disse que estava indo visitar o Museu de Arte Contemporânea Sofia Imberé. Se bem me lembro, ela tinha";
pais[1].local[3].dica[1]= "Lembro-me bem dessa pessoa. Ela estava em busca de um refrigerante chamado Maltín, que é feito do malte. Ela tinha";

pais[2] = new paises("Brasil");
pais[2].capital = "Brasília";
pais[2].info = "Seja muito bem-vindo ao Brasil! Aproveite sua estadia conhecendo os lugares da cidade. Garanto que não vai ficar entediado!";
pais[2].missao[0] = "O Cristo Redentor foi roubado. A estátua é feita de concreto armado e pedra-sabão, tem 30 metros de altura, pesa 635 toneladas, e fica no Parque Nacional da Floresta da Tijuca, no Rio de Janeiro. Siga o bandido até seu esconderijo e recupere a estátua.";
pais[2].local[4].dica[0]= "Este sujeito chegou aqui falando sobre o “Sítio do Picapau Amarelo”, que são histórias infantis. Ele possuía";
pais[2].local[1].dica[0]= "Essa pessoa chegou bastante animada, procurando por discos de Bossa Nova. Lembro-me de ela ter";
pais[2].local[2].dica[0]= "Este sujeito passou por aqui carregando uma bandeira com as cores verde, azul e amarelo. Ah, ele tinha";
pais[2].local[3].dica[0]= "Tenho certeza que este sujeito mencionou um prato chamado acarajé… Mas não faço ideia de onde ele vai conseguir isso. Ele possuía";
pais[2].local[4].dica[1]= "Essa pessoa esteve aqui para pesquisar sobre o Curupira. Eu disse que ela encontraria mais respostas se fosse à fonte. Acho que ela tinha";
pais[2].local[1].dica[1]= "Este sujeito apareceu por aqui perguntando onde poderia comprar um instrumento musical chamado berimbau. Ele tinha";
pais[2].local[2].dica[1]= "Essa pessoa veio aqui pesquisando sobre os quadros de Tarsila do Amaral. Ela tinha";
pais[2].local[3].dica[1]= "Essa pessoa veio à minha barraca com uma vontade muito grande de comer tapioca. Eu não a culpo, é uma comida deliciosa. Ela possuía";

pais[3] = new paises("Chile");
pais[3].capital = "Santiago";
pais[3].info = "Seja muito bem-vindo ao Chile! Aproveite sua estadia conhecendo os lugares da cidade. Garanto que não vai ficar entediado!";
pais[3].missao[0] = "Um dos Moai da Ilha de Páscoa foi roubado. Moai é como são conhecidas as grandes cabeças da ilha: São estátuas gigantescas, e ninguém sabe exatamente como foram construídas. É um importante patrimônio da humanidade. Siga o bandido até seu esconderijo e prenda-o.";
pais[3].local[4].dica[0]= "Esse sujeito buscava informações sobre o deserto mais seco do mundo. Acho que ele viajou para lá... Ele tinha";
pais[3].local[1].dica[0]= "Eu me lembro bem desse sujeito. Ele veio à minha loja querendo comprar discos de música Andina. Ele tinha";
pais[3].local[2].dica[0]= "Este sujeito passou por aqui carregando uma bandeira com as cores vermelho, azul e branco. Ele possuía";
pais[3].local[3].dica[0]= "Esse sujeito veio em busca de pastel de choclo. Ele disse que era um famoso prato nacional, mas não lembro de qual país... Lembro que ele tinha";
pais[3].local[4].dica[1]= "Essa pessoa queria fazer uma pesquisa extensa sobre a Catedral Metropolitana de Santiago. Se não me engano, ela tinha";
pais[3].local[1].dica[1]= "Essa pessoa veio aqui procurando músicas para dançar Cueca, um estilo de dança típico. Ela tinha";
pais[3].local[2].dica[1]= "Essa pessoa veio aqui procurando miniaturas das estátuas da Ilha de Páscoa. Ah, e ela tinha";
pais[3].local[3].dica[1]= "Essa pessoa veio à minha barraca procurando uma grande quantidade de abacate. Um tanto esquisito, não acha? Ela tinha";

pais[4] = new paises("Argentina");
pais[4].capital = "B. Aires";
pais[4].info = "Seja muito bem-vindo à Argentina! Aproveite sua estadia conhecendo os lugares da cidade. Garanto que não vai ficar entediado!";
pais[4].missao[0]= "O Obelisco de Buenos Aires é um monumento histórico da cidade de Buenos Aires, Argentina. Foi erguido na Praça da República, em comemoração ao quarto centenário da fundação da cidade. Siga o bandido até seu esconderijo e recupere este importante tesouro";
pais[4].local[4].dica[0]= "Sim, vi essa pessoa. Ela procurava livros de Ernesto Sabato e Jorge Luis Borges. Ele possuía";
pais[4].local[1].dica[0]= "Essa pessoa veio aqui à procura de discos da Nueva Canción, um estilo de música bem popular, quase folclórico. Interessante, não? Ah, e ela tinha";
pais[4].local[2].dica[0]= "Este sujeito passou por aqui carregando uma bandeira com as cores azul e branco. Ele tinha";
pais[4].local[3].dica[0]= "Esse sujeito queria muito comprar doce de leite e alfajor. Pena que eu só vendo frutas e verduras... Eu lembro que ele tinha";
pais[4].local[4].dica[1]= "Sim, esse sujeito esteve aqui pesquisando sobre José de San Martín. Ele tinha";
pais[4].local[1].dica[1]= "Lembro-me desse sujeito. Disse que gostava de Tango, um estilo musical com uma dança muito complexa e bonita. O sujeito tinha";
pais[4].local[2].dica[1]= "Eu me lembro dessa pessoa. Ela queria saber sobre o maior fóssil de dinossauro do mundo, que mede 17m de altura e 40m de comprimento. Ela tinha";
pais[4].local[3].dica[1]= "Essa pessoa passou por aqui procurando por um bom vinho. Que gosto requintado! Se não me engano, ela tinha";